﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class BulletController : MonoBehaviour {
    public Transform bulletTransform;
     Rigidbody2D rb;
    public float speed;

    //dibas
    //public Sprite Glass1;
   // public Sprite Glass2;
   // public Sprite Glass3;

   


    // Use this for initialization
    void Start () {
        rb = bulletTransform.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        
            bulletTransform.Translate(Vector3.up*20f*Time.deltaTime);
                
    }

    // void OnTriggerEnter2D(Collider2D other)
    //{

    //    Debug.Log("Colllison happens name "+other.name);

    //    StartCoroutine(ColorRoutine());
    //}
     void OnTriggerEnter2D(Collider2D other)   //void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("obstacle"))
        {
            ////Debug.Log("Bullet obstacle");
            StartCoroutine(GameOverCoroutine());
        }
        else if (other.gameObject.CompareTag("color-obstacle"))
        {
            Debug.Log("Bullet color");
            //Change the bullet color here

        }
        else
        {
            //Debug.Log("Tag Name " + other.gameObject.name);
            ////Debug.Log("Bullet corotuine");

            /*
            if (GlassController.glassController.HitCount == 0)
            {
                GlassController.glassController.Sr.sprite = Glass1;
                
            }
            else if (GlassController.glassController.HitCount == 1) {
                GlassController.glassController.Sr.sprite = Glass2;
            }
            else if (GlassController.glassController.HitCount == 2)
            {
                GlassController.glassController.Sr.sprite = Glass3;
            }
            GlassController.glassController.HitCount++;
            */
            StartCoroutine(ColorRoutine());
        }
        //StartCoroutine(ColorRoutine());
    }
    IEnumerator ColorRoutine()
    {
        CanonController.canonController.PlayParticle();
        Destroy(gameObject);
        yield return new WaitForSeconds(.1f);
        CanonController.canonController.StopParticle();
    }
    
    IEnumerator GameOverCoroutine()
    {
        Destroy(gameObject);
        yield return new WaitForSeconds(.1f);
        SceneManager.LoadScene(0);
        Debug.Log("Game Over");
        
    }
}
