﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class FishScript : MonoBehaviour {

  
    //dibas
    public Animator endmenuanim;
    public Animator OtherAnim;
    public Animator FishAnim;
    public ParticleSystem BloodP;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        BloodP.Play();
        AudioController.Instance.Play(AudioController.Instance.HURT);
        StartCoroutine("GameOverCoroutine");
        
        

    }
    IEnumerator GameOverCoroutine()
    {
        Debug.Log("Game Over routine obstacle");
        //Handheld.Vibrate();
        Vibration.Vibrate(200);
        
        CameraShaker.Instance.ShakeOnce(2f, 2f, .1f, 1f);

        LevelController.instance.setinput(false);
        LightingController.instance.All = false;
        AudioController.Instance.Stop(AudioController.Instance.GAME_PLAY);
        AudioController.Instance.Play(AudioController.Instance.EndSad);
        OtherAnim.Play("ObjectsExit", -1, 0);
        FishAnim.Play("FishDeadAnimation", -1, 0);

        yield return new WaitForSeconds(1f);
        endmenuanim.Play("EndPanel", -1, 0);
        //endanim.Play("EndPanelExit", -1, 0);


        //SceneManager.LoadScene(0);
        //Debug.Log("Game Over");

    }

}
