﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildController : MonoBehaviour {

    public List<CircleItemController> circleItemControllers;
    public static ChildController instance;
	// Use this for initialization
	void Awake () {
		
	}
	
    public void ResetCircleItemColor()
    {
        int temp = circleItemControllers.Count;
        Debug.Log("Number of color item for reset "+temp);
        for (int i = 0; i < temp; i++)
        {
            circleItemControllers[i].ResetCollideradnGlass();
        }
        //gameObject.SetActive(false);
    }

}
