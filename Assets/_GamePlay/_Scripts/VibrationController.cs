﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VibrationController : MonoBehaviour {

    public Button vibButton1;
    public Button vibButton2;
    public Button vibButton3;
	// Use this for initialization
	void Start () {

        vibButton1.onClick.AddListener(delegate
        {
            Vibration.Vibrate(150);
            Debug.Log("Vib 1");
        });
        vibButton2.onClick.AddListener(delegate
        {
            Vibration.Vibrate(150);
            Debug.Log("Vib 2");
        });
        vibButton3.onClick.AddListener(delegate
        {
            Vibration.Vibrate(150);
            Debug.Log("Vib 3");

        });
    }
	
	
}
