﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewStageGenController : MonoBehaviour
{
    int layer_1_totalDifficulty = 0;
    int layer_2_totalDifficulty = 0;

    int maximumTargetSize = 65;
    int targetSize_1 = 5;
    int targetSize_2 = 15;
    int targetSize_3 = 35;
    

    int maximumNumberofLayer = 150;
    int numberofLayer_1 = 5;
    int numberofLayer_2 = 70;
    

    int maximumLayerSize = 55;
    int layerSize_1 = 5;
    int layerSize_2 = 15;
    

    int maximumLayerSpeed = 250;
    int layerSpeed_1 = 5;
    int layerSpeed_2 = 15;
    int layerSpeed_3 = 30;
    int layerSpeed_4 = 100;
    int layerSpeed_5 = 150;

    int maximumClockTime = 200;
    int clockTime_1 = 50;
    int clockTime_2 = 100;


    int maximumLayerFlexibility = 150;


    int targetSizeValue_new = 0;
    int numberLayerValue_new = 0;

    int firstLayerSize_new = 0;
    int firstlayerSpeed_new = 0;
    int firstLayerClockTime_new = 0;
    int firstLayerFlexible_new = 0;

    int secondLayerSize_new = 0;
    int secondlayerSpeed_new = 0;
    int secondLayerClockTime_new = 0;
    int secondLayerFlexible_new = 0;

    int thirdLayerSize_new = 0;
    int thirdlayerSpeed_new = 0;
    int thirdLayerClockTime_new = 0;
    int thirdLayerFlexible_new = 0;


    int targetSizeValue_regular_new = 0;
    int numberLayerValue_regular_new = 0;


    int firstLayerSize_regular_new = 0;
    int firstlayerSpeed_regular_new = 0;
    int firstLayerClockTime_regular_new = 0;
    int firstLayerFlexible_regular_new = 0;

    int secondLayerSize_regular_new = 0;
    int secondlayerSpeed_regular_new = 0;
    int secondLayerClockTime_regular_new = 0;
    int secondLayerFlexible_regular_new = 0;

    int thirdLayerSize_regular_new = 0;
    int thirdlayerSpeed_regular_new = 0;
    int thirdLayerClockTime_regular_new = 0;
    int thirdLayerFlexible_regular_new = 0;

    public List<ObstacleController> largeThreeObstacleList;
    public List<ChildController> circleList;
    List<Difficulty> difficultiesList = new List<Difficulty>();

    Difficulty diff = new Difficulty();
    //[Range(1, 42)]

    //[Header("Total Number of Level")] 

    int totalLevel = 10;
    float first_Portionof_Levels;
    float second_Portionof_Levels;
    float third_Portionof_Levels;

    int first_Portionof_MaximumDifficulty;
    int second_Portionof_MaximumDifficulty;
    int third_Portionof_MaximumDifficulty;

    int powerIncrementPerLevel;
    int runningDifficulty;
    int runningLevelNumber = 0;
    int runningLevelPower;
    int previousLevelPower;
    int maxDifficulty = 60;
    int minimumPowerforSecondLayer = 13;
    int minimumPowerforThirdLayer = 24;

    int newCircleSize = 0;
    int totalDifficulty = 0;


    [Range(1, 4)]
    public int circleSize;
    int maxCircleSize = 4;

    [Range(1, 3)]
    public int numberofLayer = 1;
    int newNumberofLayer;

    [Header("First Layer")]
    [Range(1, 3)]
    public int firstLayerSize;
    int firstLayerMaxSize = 3;
    int newFirstLayerMaxSize;

    [Range(1, 6)]
    public int firstLayerSpeed;
    int firstLayerMaxSpeed = 6;
    int newFirstLayerMaxSpeed = 0;

    [Range(1, 3)]
    public int firstLayerclockTime;
    int firstLayerMaxClockTime = 3;
    int newFirstLayerClockTime;

    public bool firstLayerClockDirection;
    int newFirstLayerClockDirection;

    public bool firstLayerAntiClockDirection;
    int newFirstLayerAntiClockDirection;

    public bool firstLayerFlexible;
    int newFirstLayerFlexible = 0;

    [Header("Second Layer")]
    [Range(1, 3)]
    public int secondLayerSize;
    int secondLayerMaxSize = 3;
    int newSecondLayerMaxSize;

    [Range(1, 6)]
    public int secondLayerSpeed;
    int secondLayerMaxSpeed = 6;
    int newSecondLayerMaxSpeed;

    [Range(1, 3)]
    public int secondLayerclockTime;
    int newSecondLayerClockTime;

    public bool secondLayerClockDirection;
    int newSecondLayerClockDirection;

    public bool secondLayerAntiClockDirection;
    int newSecondLayerAntiClockDirection;

    public bool secondLayerFlexible;
    int newSecondLayerFlexible = 0;


    [Header("Third Layer")]
    [Range(1, 3)]
    public int thirdLayerSize;
    int thirdLayerMaxSize = 3;
    int newThirdLayerMaxSize;

    [Range(1, 6)]
    public int thirdLayerSpeed;
    int thirdLayerMaxSpeed = 6;
    int newThirdLayerMaxSpeed;

    [Range(1, 3)]
    public int thirdLayerclockTime;
    int newThirdLayerClockTime;

    public bool thirdLayerClockDirection;
    int newThirdLayerClockDirection;

    public bool thirdLayerAntiClockDirection;
    int newThirdLayerAntiClockDirection;

    public bool thirdLayerFlexible;
    int newThirdLayerFlexible = 0;



    public static NewStageGenController instance;
    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        AudioController.Instance.PlayMenuBGMusic(AudioController.Instance.MENU_BG);
        Stage_Power_Distribution_AccrodingtoLevel();
        PlayerPrefs.SetInt("runningLevel", 1);
        //GenerateCircle();
        //////StageGenValue();
        //NewStageGen();
        NewStageGenValue();
        //TestGen();
    }

    void TestGen()
    {
        Reset();
        runningLevelNumber = 6;
         if(runningLevelNumber == 1)
        {
        Debug.Log("Level 1");
        int level_1_rend = Random.Range(1, 6);
        List<int> tempList;
        tempList = Level1Power.Level_1_PowerDistribution(level_1_rend);

        GenerateCircle(tempList[0], "Test Layer");
        numberofLayer = tempList[1];
        firstLayerSize = tempList[2];
        firstLayerSpeed = tempList[3];
        Debug.Log("First Layer Speed " + firstLayerSpeed);
        NewStageGen();
         }
        if (runningLevelNumber == 2)
        {
            Debug.Log("Level 2");
            int level_2_rend = Random.Range(1, 6);
            List<int> tempList;
            tempList = Level1Power.Level_2_PowerDistribution(level_2_rend);

            if (tempList.Count == 4)
            {
                GenerateCircle(tempList[0], "Test Layer");
                numberofLayer = tempList[1];
                firstLayerSize = tempList[2];
                firstLayerSpeed = tempList[3];
            }
            else if (tempList.Count == 6)
            {
                GenerateCircle(tempList[0], "Test Layer");
                numberofLayer = tempList[1];
                firstLayerSize = tempList[2];
                firstLayerSpeed = tempList[3];
                secondLayerSize = tempList[4];
                secondLayerSpeed = tempList[5];
            }

        }
    }

    int CurrentLevelPower()
    {
       
        if (runningLevelNumber == 1)
        {
            return 50;
        }
        else if(runningLevelNumber == 2)
        {
            return 80;
        }
        else if (runningLevelNumber == 3)
        {
            return 120;
        }
        else if (runningLevelNumber == 4)
        {
            return 170;
        }
        else if (runningLevelNumber == 5)
        {
            return 230;
        }
        else if (runningLevelNumber == 6)
        {
            return 300;
        }
        else if (runningLevelNumber == 7)
        {
            return 380;
        }
        else if (runningLevelNumber == 8)
        {
            return 470;
        }
        else if (runningLevelNumber == 9)
        {
            return 570;
        }
        else if (runningLevelNumber == 10)
        {
            return 650;
        }
        else
        {
            return 50;
        }
    }


    int NextLevelPower()
    {

        if (runningLevelNumber == 1)
        {
            return 80;
        }
        else if (runningLevelNumber == 2)
        {
            return 120;
        }
        else if (runningLevelNumber == 3)
        {
            return 170;
        }
        else if (runningLevelNumber == 4)
        {
            return 230;
        }
        else if (runningLevelNumber == 5)
        {
            return 300;
        }
        else if (runningLevelNumber == 6)
        {
            return 380;
        }
        else if (runningLevelNumber == 7)
        {
            return 470;
        }
        else if (runningLevelNumber == 8)
        {
            return 570;
        }
        else if (runningLevelNumber == 9)
        {
            return 650;
        }
        else if (runningLevelNumber == 10)
        {
            return 750;
        }
        else
        {
            return 50;
        }
    }

    int LayerFlexibility()
    {
        if (runningLevelNumber == 5)
        {
            
            if (Random.Range(1, 9) <= 5)
            {
                return 150;
            }
            else
            {
                return 0;
            }
            

        }
        else if(runningLevelNumber == 6)
        {
            if (Random.Range(1, 9) <= 5)
            {
                return 150;
            }
            else
            {
                return 0;
            }
        }
        else if (runningLevelNumber == 7)
        {
            if (Random.Range(1, 9) <= 6)
            {
                return 150;
            }
            else
            {
                return 0;
            }
        }
        else if (runningLevelNumber == 8)
        {
            if (Random.Range(1, 9) <= 6)
            {
                return 150;
            }
            else
            {
                return 0;
            }
        }
        else if (runningLevelNumber >= 9)
        {
            if (Random.Range(1, 9) <= 7)
            {
                return 150;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }
    void AssignGenerationValue_First_Layer(int listIndex)
    {
        ////Debug.Log("Assign Value Func Number of Layer_______ " + numberofLayer);
        //firstLayerFlexible = difficultiesList[listIndex].layerFlexibility;
        // Debug.Log("Assign Value Func Number of Layer_______ " + numberofLayer);

        firstLayerSize = difficultiesList[listIndex].layerMaxSize;
        ////Debug.Log("Assign Value Func first Layer Max Size_______ " + firstLayerSize);

        firstLayerSpeed = difficultiesList[listIndex].layerMaxSpeed;
        //Debug.Log("Assign Value Func Layer Max Speed_______ " + firstLayerSpeed);

        if (firstLayerClockDirection && firstLayerAntiClockDirection)
        {
            //newFirstLayerClockTime = ClockTiming(clockTimingTemp);
            //Debug.Log(" New First Layer Clock Time================= " + newFirstLayerClockTime);
            firstLayerclockTime = newFirstLayerClockTime; //difficultiesList[listIndex].layerclockTime;
        }

        //Debug.Log("Assign Value Func Max Clock Time _______ " + firstLayerclockTime);
        //Debug.Log("Assign Value Total Difficulty________________ " + difficultiesList[listIndex].layerTotalDifficulty);

        difficultiesList.Clear();
        //Debug.Log("List value after clean it-------------------------------------------------- " + difficultiesList.Count);
        //NewStageGen();
    }

    int CircleRegularValue(int listIndex)
    {
        if (difficultiesList[listIndex].circleSize == targetSize_1)
        {
            return 1;
        }
        else if(difficultiesList[listIndex].circleSize == targetSize_2)
        {
            return 2;
        }
        else if (difficultiesList[listIndex].circleSize == targetSize_3)
        {
            return 3;
        }
        else if (difficultiesList[listIndex].circleSize == maximumTargetSize)
        {
            return 4;
        }
        else
        {
            return 1;
        }
    }


    int NumberofLayerRegularValue(int listIndex)
    {
        if (difficultiesList[listIndex].numberofLayer == numberofLayer_1)
        {
            return 1;
        }
        else if (difficultiesList[listIndex].numberofLayer == numberofLayer_2)
        {
            return 2;
        }
        else if (difficultiesList[listIndex].numberofLayer == maximumNumberofLayer)
        {
            return 3;
        }        
        else
        {
            return 1;
        }
    }


    int LayerSizeRegularValue(int listIndex)
    {
        if (difficultiesList[listIndex].layerMaxSize == layerSize_1)
        {
            return 1;
        }
        else if (difficultiesList[listIndex].layerMaxSize == layerSize_2)
        {
            return 2;
        }
        else if (difficultiesList[listIndex].layerMaxSize == maximumLayerSize)
        {
            return 3;
        }
        else
        {
            return 1;
        }
    }


    int LayerSpeedRegularValue(int listIndex)
    {
        if (difficultiesList[listIndex].layerMaxSpeed == layerSpeed_1)
        {
            return 1;
        }
        else if (difficultiesList[listIndex].layerMaxSpeed == layerSpeed_2)
        {
            return 2;
        }
        else if (difficultiesList[listIndex].layerMaxSpeed == layerSpeed_3)
        {
            return 3;
        }
        else if (difficultiesList[listIndex].layerMaxSpeed == layerSpeed_4)
        {
            return 4;
        }
        else if (difficultiesList[listIndex].layerMaxSpeed == layerSpeed_5)
        {
            return 5;
        }
        else if (difficultiesList[listIndex].layerMaxSpeed == maximumLayerSpeed)
        {
            return 6;
        }
        else
        {
            return 1;
        }
    }

    int LayerClockTimeRegularValue(int listIndex)
    {
        if (difficultiesList[listIndex].layerclockTime == clockTime_1)
        {
            return 1;
        }
        else if (difficultiesList[listIndex].layerclockTime == clockTime_2)
        {
            return 2;
        }
        else if (difficultiesList[listIndex].layerclockTime == maximumClockTime)
        {
            return 3;
        }
        else
        {
            return 0;
        }
    }

    void AssignGenerationValue_First_Layer_New(int listIndex)
    {
        //Debug.Log("Generation - 1");
        layer_1_totalDifficulty = difficultiesList[listIndex].layerTotalDifficulty;
        Debug.Log("Total Difficulty Generation - 1 " + layer_1_totalDifficulty);
        
        GenerateCircle(CircleRegularValue(listIndex), "New Layer");
        Debug.Log("Circle Value " + CircleRegularValue(listIndex));

        numberofLayer = NumberofLayerRegularValue(listIndex);
       Debug.Log("Number of Layer " + numberofLayer);

        firstLayerSize = LayerSizeRegularValue(listIndex);
        Debug.Log("Layer Size " + firstLayerSize);

        firstLayerSpeed = LayerSpeedRegularValue(listIndex);
        Debug.Log("Layer Speed " + firstLayerSpeed);

        if (LayerClockTimeRegularValue(listIndex) > 0)
        {
            firstLayerclockTime = LayerClockTimeRegularValue(listIndex);
            Debug.Log("Layer Clock Time " + firstLayerSpeed);
            Debug.Log("Layer Clock Time " + firstLayerSpeed);
            firstLayerFlexible = true;
        }
        else
        {
            firstLayerFlexible = false;
        }
        if (numberofLayer == 1)
        {
            Debug.Log("New Stage Gen 1");
            NewStageGen();

        }
        difficultiesList.Clear();
        
    }


    void AssignGenerationValue_Second_Layer_New(int listIndex)
    {
        layer_2_totalDifficulty = difficultiesList[listIndex].layerTotalDifficulty;
        Debug.Log("Total Difficulty second Layer " + layer_2_totalDifficulty);
        
        secondLayerSize = LayerSizeRegularValue(listIndex);
        Debug.Log("Layer Size " + secondLayerSize);

        secondLayerSpeed = LayerSpeedRegularValue(listIndex);
        Debug.Log("Layer Speed " + secondLayerSpeed);

        if (LayerClockTimeRegularValue(listIndex) > 0)
        {
            secondLayerclockTime = LayerClockTimeRegularValue(listIndex);
            secondLayerFlexible = true;
        }
        else
        {
            secondLayerFlexible = false;
        }
        if (numberofLayer == 2)
        {
            Debug.Log("New stage gen 2 ");
            NewStageGen();
        }
        
        difficultiesList.Clear();
        
    }

    void AssignGenerationValue_Third_Layer_New(int listIndex)
    {

        Debug.Log("Total Difficulty " + difficultiesList[listIndex].layerTotalDifficulty);



        thirdLayerSize = LayerSizeRegularValue(listIndex);
        Debug.Log("Layer Size " + thirdLayerSize);

        thirdLayerSpeed = LayerSpeedRegularValue(listIndex);
        Debug.Log("Layer Speed " + thirdLayerSpeed);

        if (LayerClockTimeRegularValue(listIndex) > 0)
        {
            thirdLayerclockTime = LayerClockTimeRegularValue(listIndex);
            thirdLayerFlexible = true;
        }
        else
        {
            thirdLayerFlexible = false;
        }
        if (numberofLayer == 3)
        {
            Debug.Log("New stage gen 3 ");
            NewStageGen();
        }
        difficultiesList.Clear();
        
    }

    void AssignGenerationValue_Second_Layer(int listIndex)
    {
        // numberofLayer = difficultiesList[listIndex].layerNumber;
        //Debug.Log("Assign Value Func Number of Layer_______ " + numberofLayer);

        ////Debug.Log("List value befor clean it-------------------------------------------------- ||||||||||||||||||||||||||||||||||||||" + difficultiesList.Count);



        ////Debug.Log("Assign Value Func first Layer Max Size 2nd Layer_______ " + secondLayerSize);

        secondLayerSpeed = difficultiesList[listIndex].layerMaxSpeed;
        ////Debug.Log("Assign Value Func Layer Max Speed 2nd Layer_______ " + secondLayerSpeed);

        if (secondLayerClockDirection && secondLayerAntiClockDirection)
        {
            //newFirstLayerClockTime = ClockTiming(clockTimingTemp);
            //Debug.Log(" New First Layer Clock Time================= " + newFirstLayerClockTime);
            secondLayerclockTime = newSecondLayerClockTime; //difficultiesList[listIndex].layerclockTime;
        }
        difficultiesList.Clear();
        ///Debug.Log("Assign Value Func Max Clock Time 2nd Layer_______ " + firstLayerclockTime);

        //Debug.Log("Assign Value Total Difficulty 2nd Layer________________ " + difficultiesList[listIndex].layerTotalDifficulty);

        // NewStageGen();
    }

    void AssignGenerationValue_Third_Layer(int listIndex)
    {
        // numberofLayer = difficultiesList[listIndex].layerNumber;
        //Debug.Log("Assign Value Func Number of Layer_______ " + numberofLayer);

        ////Debug.Log("List value befor clean it-------------------------------------------------- ||||||||||||||||||||||||||||||||||||||" + difficultiesList.Count);



        ////Debug.Log("Assign Value Func first Layer Max Size 2nd Layer_______ " + secondLayerSize);

        thirdLayerSpeed = difficultiesList[listIndex].layerMaxSpeed;
        ////Debug.Log("Assign Value Func Layer Max Speed 2nd Layer_______ " + secondLayerSpeed);

        if (thirdLayerClockDirection && thirdLayerAntiClockDirection)
        {
            //newFirstLayerClockTime = ClockTiming(clockTimingTemp);
            //Debug.Log(" New First Layer Clock Time================= " + newFirstLayerClockTime);
            thirdLayerclockTime = newThirdLayerClockTime; //difficultiesList[listIndex].layerclockTime;
        }
        difficultiesList.Clear();

        ///Debug.Log("Assign Value Func Max Clock Time 2nd Layer_______ " + firstLayerclockTime);

        //Debug.Log("Assign Value Total Difficulty 2nd Layer________________ " + difficultiesList[listIndex].layerTotalDifficulty);

        // NewStageGen();
    }

    void Stage_Power_Distribution_AccrodingtoLevel()
    {
        first_Portionof_Levels = (totalLevel * 30) / 100f;
        ////Debug.Log("FPL "+first_Portionof_Levels);

        second_Portionof_Levels = (totalLevel * 30) / 100f;
        second_Portionof_Levels = first_Portionof_Levels + second_Portionof_Levels;
        ////Debug.Log("SPL "+second_Portionof_Levels);

        third_Portionof_Levels = (totalLevel * 40) / 100f;
        third_Portionof_Levels = first_Portionof_Levels + third_Portionof_Levels;
        ////Debug.Log("TPL " + third_Portionof_Levels);

        first_Portionof_MaximumDifficulty = (maxDifficulty * 30) / 100;
        ////Debug.Log("FPMD "+first_Portionof_MaximumDifficulty);
        second_Portionof_MaximumDifficulty = (maxDifficulty * 30) / 100;
        ////Debug.Log("SPMD " + second_Portionof_MaximumDifficulty);
        third_Portionof_MaximumDifficulty = (maxDifficulty * 40) / 100;
        ////Debug.Log("TPMD " + third_Portionof_MaximumDifficulty);

        powerIncrementPerLevel = first_Portionof_MaximumDifficulty / (int)first_Portionof_Levels;
        ////Debug.Log("PIPL " + powerIncrementPerLevel);

        //////PlayerPrefs.SetInt("runningLevel",1);
        /////runningLevelNumber = PlayerPrefs.GetInt("runningLevel");
        ////Debug.Log("Running Level Number " + runningLevelNumber);
    }
    void GenerateCircle(int n, string layerName)
    {

        circleList[n - 1].gameObject.SetActive(true);
        //for(int i=0; i<circleList[n-1].gameObject.transform.childCount; i++)
        //{
        //    circleList[n - 1].gameObject.transform.GetChild(i).gameObject.SetActive(true);
        //}
        //// Debug.Log("Layer Name !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! " + layerName);
    }


    int FlexibilityFirstLayer(int randomValue)
    {
        if (randomValue <= 4)
        {
            firstLayerFlexible = false;
            return 0;
        }
        else
        {
            firstLayerFlexible = true;
            return 3;
        }

    }

    int FlexibilitySecondLayer(int randomValue)
    {
        if (randomValue <= 4)
        {
            secondLayerFlexible = false;
            return 0;
        }
        else
        {
            secondLayerFlexible = true;
            return 3;
        }

    }

    int FlexibilityThirdLayer(int randomValue)
    {
        if (randomValue <= 4)
        {
            thirdLayerFlexible = false;
            return 0;
        }
        else
        {
            thirdLayerFlexible = true;
            return 3;
        }

    }

    int ClockDirectionFirstLayer(int clockDirectionTemp)
    {
        if (clockDirectionTemp == 4)
        {
            clockDirectionTemp = 3;
        }

        if (clockDirectionTemp == 1)
        {
            firstLayerClockDirection = true;
            firstLayerAntiClockDirection = false;
            //newFirstLayerClockDirection++;
            return 1;
        }
        else if (clockDirectionTemp == 2)
        {
            firstLayerClockDirection = false;
            firstLayerAntiClockDirection = true;
            //newFirstLayerClockDirection = 1;
            return 1;
        }
        else if (clockDirectionTemp == 3)
        {
            firstLayerClockDirection = true;
            firstLayerAntiClockDirection = true;

            //newFirstLayerClockDirection = 1;
            return 2;
        }
        return 1;
    }



    int ClockDirectionSecondLayer(int clockDirectionTemp)
    {
        if (clockDirectionTemp == 4)
        {
            clockDirectionTemp = 3;
        }

        if (clockDirectionTemp == 1)
        {
            secondLayerClockDirection = true;
            secondLayerAntiClockDirection = false;
            //newFirstLayerClockDirection++;
            return 1;
        }
        else if (clockDirectionTemp == 2)
        {
            secondLayerClockDirection = false;
            secondLayerAntiClockDirection = true;
            //newFirstLayerClockDirection = 1;
            return 1;
        }
        else if (clockDirectionTemp == 3)
        {
            secondLayerClockDirection = true;
            secondLayerAntiClockDirection = true;

            //newFirstLayerClockDirection = 1;
            return 2;
        }
        return 1;
    }

    int ClockDirectionThirdLayer(int clockDirectionTemp)
    {
        if (clockDirectionTemp == 4)
        {
            clockDirectionTemp = 3;
        }

        if (clockDirectionTemp == 1)
        {
            thirdLayerClockDirection = true;
            thirdLayerAntiClockDirection = false;
            //newFirstLayerClockDirection++;
            return 1;
        }
        else if (clockDirectionTemp == 2)
        {
            thirdLayerClockDirection = false;
            thirdLayerAntiClockDirection = true;
            //newFirstLayerClockDirection = 1;
            return 1;
        }
        else if (clockDirectionTemp == 3)
        {
            thirdLayerClockDirection = true;
            thirdLayerAntiClockDirection = true;

            //newFirstLayerClockDirection = 1;
            return 2;
        }
        return 1;
    }

    int ClockTiming(int clockDirectionTiming)
    {
        if (clockDirectionTiming == 4)
        {
            clockDirectionTiming = 3;
        }
        if (clockDirectionTiming == 1)
        {
            return 1;
        }
        else if (clockDirectionTiming == 2)
        {
            return 2;
        }
        else if (clockDirectionTiming == 3)
        {
            return 3;
        }
        return 1;
    }

    public void StageGenValue()
    {
        Reset();
        /////////////runningLevelNumber = PlayerPrefs.GetInt("runningLevel");
        runningLevelNumber = 1;
        int powerDistribution = 0;
        int layer2Power = 7;
        int layer3Power = 14;
        int minimumPowerforPerLevel = 8;
        int minimumCircleSize = 1;
        int estimatedCirclePower = 0;
        int randomValueForLastLayer = 1;

        //if (runningLevelNumber<=first_Portionof_Levels) {


        if (runningLevelNumber == 1)
        {
            runningLevelPower = powerIncrementPerLevel + 2;
            // newNumberofLayer = 1;
            //CalculationforLayerOne("Number One");
        }
        else
        {

            runningLevelPower = runningLevelNumber * powerIncrementPerLevel;
            Debug.Log("Running Level Power  " + runningLevelPower);
        }

        if(runningLevelNumber>=5 && runningLevelNumber <= 6)
        {
            randomValueForLastLayer = Random.Range(1, 9);

        }
        else if (runningLevelNumber > 6)
        {
            randomValueForLastLayer = 5;
        }
        if (runningLevelPower <= minimumPowerforSecondLayer)
        {
            newNumberofLayer = 1;
            numberofLayer = newNumberofLayer;
            //Pls number of layer take out from script
            int circleTemp = Random.Range(3, 5);
            circleTemp = circleTemp >= 4 ? circleTemp = 4 : circleTemp = 3;
            //Debug.Log("Cicrle Temp Value " + circleTemp);
            GenerateCircle(circleTemp, "Layer ONE");
            newCircleSize = circleTemp;

            CalculationforLayerOne("Number One", runningLevelPower, false);
            NewStageGen();

        }
        else if (runningLevelPower > minimumPowerforSecondLayer && randomValueForLastLayer<=4)
        {
            int randomLayerNumber = Random.Range(1, 3);
            randomLayerNumber = randomLayerNumber >= 2 ? randomLayerNumber = 2 : randomLayerNumber = 1;


            if (randomLayerNumber == 1 && runningLevelNumber <= 3)
            {
                int circleTemp = Random.Range(3, 5);
                circleTemp = circleTemp >= 4 ? circleTemp = 4 : circleTemp = 3;
                Debug.Log("Cicrle Temp Value " + circleTemp);
                GenerateCircle(circleTemp, "Layer ONE");
                newCircleSize = circleTemp;

                newNumberofLayer = 1;
                numberofLayer = newNumberofLayer;
                CalculationforLayerOne("Number two ", runningLevelPower, false);

                NewStageGen();
            }
            else
            {

                newNumberofLayer = 2;
                numberofLayer = newNumberofLayer;
                powerDistribution = runningLevelPower - layer2Power;
                powerDistribution = powerDistribution - minimumCircleSize;
                Debug.Log("Power Distribution " + powerDistribution);
                if (powerDistribution < (minimumPowerforPerLevel * 2))
                {
                    powerDistribution = minimumPowerforPerLevel * 2;
                    estimatedCirclePower = 1;
                    GenerateCircle(estimatedCirclePower, "Layer ONE");

                }

                else
                {

                    int circleTemp = Random.Range(1, 5);
                    circleTemp = circleTemp >= 4 ? circleTemp = 4 : circleTemp;
                    Debug.Log("Cicrle Temp Value " + circleTemp);
                    GenerateCircle(circleTemp, "Layer ONE");
                    newCircleSize = circleTemp;

                    powerDistribution = (powerDistribution + 1) - newCircleSize;
                }

                CalculationforLayerOne("Number tWo eLsE", powerDistribution / 2, true);
                CalculationforLayerTwo(powerDistribution / 2, true);

                NewStageGen();


            }
        }
        else if(runningLevelPower>minimumPowerforSecondLayer && randomValueForLastLayer>=5)
        {
            newNumberofLayer = 4;
            numberofLayer = newNumberofLayer;
            powerDistribution = runningLevelPower - layer3Power;

            if (runningLevelNumber == 5)
            {
                int circleTemp = 1;                     
                GenerateCircle(circleTemp, "Layer ONE");
                newCircleSize = circleTemp;
                powerDistribution = (powerDistribution  - newCircleSize);         
            }
            else
            {
                int circleTemp = Random.Range(1, 5);
                circleTemp = circleTemp >= 4 ? circleTemp = 4 : circleTemp;
                Debug.Log("Cicrle Temp Value " + circleTemp);
                GenerateCircle(circleTemp, "Layer ONE");
                newCircleSize = circleTemp;

                powerDistribution = powerDistribution  - newCircleSize;
            }

            CalculationforLayerOne("Number three if", powerDistribution / 3, true);
            CalculationforLayerTwo(powerDistribution / 3, true);
            CalculationforLayerThree(powerDistribution / 3, true);
            NewStageGen();
        }
        //}



    }

    public void NewStageGenValue()
    {
        Reset();
        runningLevelNumber = PlayerPrefs.GetInt("runningLevel");
        //runningLevelNumber =10 ;
        /**
        int powerDistribution = 0;
        int layer2Power = 7;
        int layer3Power = 14;
        int minimumPowerforPerLevel = 8;
        int minimumCircleSize = 1;
        int estimatedCirclePower = 0;
        int randomValueForLastLayer = 1;

        //if (runningLevelNumber<=first_Portionof_Levels) {
        

        if (runningLevelNumber == 1)
        {
            runningLevelPower = powerIncrementPerLevel + 2;
            // newNumberofLayer = 1;
            //CalculationforLayerOne("Number One");
        }
        else
        {

            runningLevelPower = runningLevelNumber * powerIncrementPerLevel;
            Debug.Log("Running Level Power  " + runningLevelPower);
        }

        if (runningLevelNumber >= 5 && runningLevelNumber <= 6)
        {
            randomValueForLastLayer = Random.Range(1, 9);

        }
        else if (runningLevelNumber > 6)
        {
            randomValueForLastLayer = 5;
        }
        if (runningLevelPower <= minimumPowerforSecondLayer)
        {
            newNumberofLayer = 1;
            numberofLayer = newNumberofLayer;
            //Pls number of layer take out from script
            int circleTemp = Random.Range(3, 5);
            circleTemp = circleTemp >= 4 ? circleTemp = 4 : circleTemp = 3;
            //Debug.Log("Cicrle Temp Value " + circleTemp);
            GenerateCircle(circleTemp, "Layer ONE");
            newCircleSize = circleTemp;

            CalculationforLayerOne("Number One", runningLevelPower, false);
            NewStageGen();

        }
        else if (runningLevelPower > minimumPowerforSecondLayer && randomValueForLastLayer <= 4)
        {
            int randomLayerNumber = Random.Range(1, 3);
            randomLayerNumber = randomLayerNumber >= 2 ? randomLayerNumber = 2 : randomLayerNumber = 1;


            if (randomLayerNumber == 1 && runningLevelNumber <= 3)
            {
                int circleTemp = Random.Range(3, 5);
                circleTemp = circleTemp >= 4 ? circleTemp = 4 : circleTemp = 3;
                Debug.Log("Cicrle Temp Value " + circleTemp);
                GenerateCircle(circleTemp, "Layer ONE");
                newCircleSize = circleTemp;

                newNumberofLayer = 1;
                numberofLayer = newNumberofLayer;
                CalculationforLayerOne("Number two ", runningLevelPower, false);

                NewStageGen();
            }
            else
            {

                newNumberofLayer = 2;
                numberofLayer = newNumberofLayer;
                powerDistribution = runningLevelPower - layer2Power;
                powerDistribution = powerDistribution - minimumCircleSize;
                Debug.Log("Power Distribution " + powerDistribution);
                if (powerDistribution < (minimumPowerforPerLevel * 2))
                {
                    powerDistribution = minimumPowerforPerLevel * 2;
                    estimatedCirclePower = 1;
                    GenerateCircle(estimatedCirclePower, "Layer ONE");

                }

                else
                {

                    int circleTemp = Random.Range(1, 5);
                    circleTemp = circleTemp >= 4 ? circleTemp = 4 : circleTemp;
                    Debug.Log("Cicrle Temp Value " + circleTemp);
                    GenerateCircle(circleTemp, "Layer ONE");
                    newCircleSize = circleTemp;

                    powerDistribution = (powerDistribution + 1) - newCircleSize;
                }

                CalculationforLayerOne("Number tWo eLsE", powerDistribution / 2, true);
                CalculationforLayerTwo(powerDistribution / 2, true);

                NewStageGen();


            }
        }
        else if (runningLevelPower > minimumPowerforSecondLayer && randomValueForLastLayer >= 5)
        {
            newNumberofLayer = 4;
            numberofLayer = newNumberofLayer;
            powerDistribution = runningLevelPower - layer3Power;

            if (runningLevelNumber == 5)
            {
                int circleTemp = 1;
                GenerateCircle(circleTemp, "Layer ONE");
                newCircleSize = circleTemp;
                powerDistribution = (powerDistribution - newCircleSize);
            }
            else
            {
                int circleTemp = Random.Range(1, 5);
                circleTemp = circleTemp >= 4 ? circleTemp = 4 : circleTemp;
                Debug.Log("Cicrle Temp Value " + circleTemp);
                GenerateCircle(circleTemp, "Layer ONE");
                newCircleSize = circleTemp;

                powerDistribution = powerDistribution - newCircleSize;
            }

            CalculationforLayerOne("Number three if", powerDistribution / 3, true);
            CalculationforLayerTwo(powerDistribution / 3, true);
            CalculationforLayerThree(powerDistribution / 3, true);
            NewStageGen();
        }
        //}

    **/
        NewCalculationforLayerOne();

    }


    void NewCalculationforLayerOne()
    {
        for(int i=0; i<4; i ++)
        {
            
            if (i == 0)
            {
                targetSizeValue_new = targetSize_1;
            }
            else if (i == 1)
            {
                targetSizeValue_new = targetSize_2;
            }
            else if (i == 2)
            {
                targetSizeValue_new = targetSize_3;
            }
            else if (i == 3)
            {
                targetSizeValue_new = maximumTargetSize;
            }
            for (int j = 0; j < 3; j++)
            {
                if (j == 0)
                {
                    numberLayerValue_new = numberofLayer_1;
                }
                else if (j == 1)
                {
                    numberLayerValue_new = numberofLayer_2;
                }
                else if (j == 2)
                {
                    numberLayerValue_new = maximumNumberofLayer;
                }
               
                for (int k = 0; k < 3; k++)
                {
                    if (k == 0)
                    {
                        firstLayerSize_new = layerSize_1;
                    }
                    else if (k == 1)
                    {
                        firstLayerSize_new = layerSize_2;
                    }
                    else if (k == 2)
                    {
                        firstLayerSize_new = maximumLayerSize;
                    }
                    for (int l = 0; l < 6; l++)
                    {
                        if (l == 0)
                        {
                            firstlayerSpeed_new = layerSpeed_1;
                        }
                        else if (l == 1)
                        {
                            firstlayerSpeed_new = layerSpeed_2;
                        }
                        else if (l == 2)
                        {
                            firstlayerSpeed_new = layerSpeed_3;
                        }
                        else if (l == 3)
                        {
                            firstlayerSpeed_new = layerSpeed_4;
                        }
                        else if (l == 4)
                        {
                            firstlayerSpeed_new = layerSpeed_5;
                        }
                        else if (l == 5)
                        {
                            firstlayerSpeed_new = maximumLayerSpeed;
                        }
                        
                        for (int m = 0; m < 3; m++)
                        {
                            if (m == 0 && runningLevelNumber>=5)
                            {
                                firstLayerClockTime_new = clockTime_1+ LayerFlexibility();
                                firstLayerClockDirection = true;
                                firstLayerAntiClockDirection = true;

                            }
                            else if (m == 1 && runningLevelNumber >= 6)
                            {
                                firstLayerClockTime_new = clockTime_2 + LayerFlexibility();
                                firstLayerClockDirection = true;
                                firstLayerAntiClockDirection = true;
                            }
                            else if (m == 2 && runningLevelNumber >= 7)
                            {
                                firstLayerClockTime_new = maximumClockTime + LayerFlexibility();
                                firstLayerClockDirection = true;
                                firstLayerAntiClockDirection = true;
                            }
                            else
                            {

                                firstLayerClockTime_new = 0;
                                int clockRotation = Random.Range(1, 9);
                                if (clockRotation <= 4)
                                {
                                    firstLayerClockDirection = true;
                                    firstLayerAntiClockDirection = false;
                                }
                                else
                                {
                                    firstLayerClockDirection = false;
                                    firstLayerAntiClockDirection = true;
                                }
                            }
                            // Write Functional Desription Here

                            totalDifficulty = targetSizeValue_new + numberLayerValue_new + firstLayerSize_new + firstlayerSpeed_new + firstLayerClockTime_new;
                            
                            //Debug.Log("Total Difficulty "+ totalDifficulty);
                            // Debug.Log("Current Level Power "+ CurrentLevelPower());
                            // Debug.Log("Next Level Power "+ NextLevelPower());


                            if (totalDifficulty>=CurrentLevelPower() && totalDifficulty < NextLevelPower())
                            {
                                if (runningLevelNumber >= 3 && runningLevelNumber <= 6)
                                {
                                    Debug.Log("Come Here ");
                                    if (numberLayerValue_new == 70)
                                    {                                       
                                        //Debug.Log("All New Difficulty --------------------------------------------------------" + totalDifficulty);
                                        diff.circleSize = targetSizeValue_new;
                                        //Debug.Log("All New targetSizeValue_new ****************************************" + targetSizeValue_new);

                                        diff.numberofLayer = numberLayerValue_new;
                                        //Debug.Log("All New numberLayerValue_new ****************************************" + numberLayerValue_new);


                                        diff.layerFlexibility = firstLayerClockTime_new;
                                        //Debug.Log("All New firstLayerClockTime_new ****************************************" + firstLayerClockTime_new);

                                        diff.layerMaxSize = firstLayerSize_new;
                                        //Debug.Log("All New firstLayerSize_new ****************************************" + firstLayerSize_new);

                                        diff.layerMaxSpeed = firstlayerSpeed_new;
                                        //Debug.Log("All New firstlayerSpeed_new ****************************************" + firstlayerSpeed_new);


                                        diff.layerclockTime = firstLayerClockTime_new;
                                        //Debug.Log("All New firstLayerClockTime_new ****************************************" + firstLayerClockTime_new);
                                        //diff.layerClockDirection = newFirstLayerClockDirection;
                                        diff.layerTotalDifficulty = totalDifficulty;

                                        difficultiesList.Add(diff);
                                    }
                                }


                               else if (runningLevelNumber >= 7 && runningLevelNumber <= 10)
                                {
                                    //Debug.Log("Come Here ");
                                    if (numberLayerValue_new == 150)
                                    {
                                        //Debug.Log("All New Difficulty --------------------------------------------------------" + totalDifficulty);
                                        diff.circleSize = targetSizeValue_new;
                                        //Debug.Log("All New targetSizeValue_new ****************************************" + targetSizeValue_new);

                                        diff.numberofLayer = numberLayerValue_new;
                                        //Debug.Log("All New numberLayerValue_new ****************************************" + numberLayerValue_new);


                                        diff.layerFlexibility = firstLayerClockTime_new;
                                        //Debug.Log("All New firstLayerClockTime_new ****************************************" + firstLayerClockTime_new);

                                        diff.layerMaxSize = firstLayerSize_new;
                                        //Debug.Log("All New firstLayerSize_new ****************************************" + firstLayerSize_new);

                                        diff.layerMaxSpeed = firstlayerSpeed_new;
                                        //Debug.Log("All New firstlayerSpeed_new ****************************************" + firstlayerSpeed_new);


                                        diff.layerclockTime = firstLayerClockTime_new;
                                        //Debug.Log("All New firstLayerClockTime_new ****************************************" + firstLayerClockTime_new);
                                        //diff.layerClockDirection = newFirstLayerClockDirection;
                                        diff.layerTotalDifficulty = totalDifficulty;

                                        difficultiesList.Add(diff);
                                    }
                                }

                                else                          
                                {
                                    if(numberLayerValue_new < 70)
                                    {
                                        Debug.Log("Come Here Less Than 70 ");
                                        //Debug.Log("All New Difficulty --------------------------------------------------------" + totalDifficulty);
                                        diff.circleSize = targetSizeValue_new;
                                        //Debug.Log("All New targetSizeValue_new ****************************************" + targetSizeValue_new);

                                        diff.numberofLayer = numberLayerValue_new;
                                        //Debug.Log("All New numberLayerValue_new ****************************************" + numberLayerValue_new);


                                        diff.layerFlexibility = firstLayerClockTime_new;
                                        //Debug.Log("All New firstLayerClockTime_new ****************************************" + firstLayerClockTime_new);

                                        diff.layerMaxSize = firstLayerSize_new;
                                        //Debug.Log("All New firstLayerSize_new ****************************************" + firstLayerSize_new);

                                        diff.layerMaxSpeed = firstlayerSpeed_new;
                                        //Debug.Log("All New firstlayerSpeed_new ****************************************" + firstlayerSpeed_new);


                                        diff.layerclockTime = firstLayerClockTime_new;
                                        //Debug.Log("All New firstLayerClockTime_new ****************************************" + firstLayerClockTime_new);
                                        //diff.layerClockDirection = newFirstLayerClockDirection;
                                        diff.layerTotalDifficulty = totalDifficulty;

                                        difficultiesList.Add(diff);
                                    }
                                    
                                }
                               
                            }
                        }

                    }
                }
            }
        }
        int randomListIndex = Random.Range(1, difficultiesList.Count);
        if (randomListIndex == difficultiesList.Count)
        {
            randomListIndex = difficultiesList.Count - 1;
        }

        int tempLayerNumber = difficultiesList[randomListIndex].numberofLayer;
        int tempTargetSize = difficultiesList[randomListIndex].circleSize;

        AssignGenerationValue_First_Layer_New(randomListIndex);

        if (tempLayerNumber == numberofLayer_2)
        {
            Debug.Log("Path to layer two "+(tempTargetSize+ tempLayerNumber));
            NewCalculationforLayerTwo(tempTargetSize, tempLayerNumber);
        }
        if (tempLayerNumber == maximumNumberofLayer)
        {
            Debug.Log("Path to layer two " + (tempTargetSize + tempLayerNumber));
            NewCalculationforLayerTwo(tempTargetSize, tempLayerNumber);

            Debug.Log("Path to layer three "+(tempTargetSize + tempLayerNumber));
            NewCalculationforLayerThree(tempTargetSize, tempLayerNumber);
        }
    }

    //Begin another

    void NewCalculationforLayerTwo(int targetSize, int layerNumber)
    {
        Debug.Log("layer two calculation Begin");
        
                for (int k = 0; k < 3; k++)
                {
                    if (k == 0)
                    {
                        secondLayerSize_new = layerSize_1;
                    }
                    else if (k == 1)
                    {
                        secondLayerSize_new = layerSize_2;
                    }
                    else if (k == 2)
                    {
                        secondLayerSize_new = maximumLayerSize;
                    }
                    for (int l = 0; l < 6; l++)
                    {
                        if (l == 0)
                        {
                            secondlayerSpeed_new = layerSpeed_1;
                        }
                        else if (l == 1)
                        {
                            secondlayerSpeed_new = layerSpeed_2;
                        }
                        else if (l == 2)
                        {
                            secondlayerSpeed_new = layerSpeed_3;
                        }
                        else if (l == 3)
                        {
                            secondlayerSpeed_new = layerSpeed_4;
                        }
                        else if (l == 4)
                        {
                            secondlayerSpeed_new = layerSpeed_5;
                        }
                        else if (l == 5)
                        {
                            secondlayerSpeed_new = maximumLayerSpeed;
                        }

                        for (int m = 0; m < 3; m++)
                        {
                            if (m == 0 && runningLevelNumber >= 5)
                            {
                                secondLayerClockTime_new = clockTime_1 + LayerFlexibility();
                                secondLayerClockDirection = true;
                                secondLayerAntiClockDirection = true;

                            }
                            else if (m == 1 && runningLevelNumber >= 6)
                            {
                                secondlayerSpeed_new = clockTime_2 + LayerFlexibility();
                                secondLayerClockDirection = true;
                                secondLayerAntiClockDirection = true;
                            }
                            else if (m == 2 && runningLevelNumber >= 7)
                            {
                                secondlayerSpeed_new = maximumClockTime + LayerFlexibility();
                                secondLayerClockDirection = true;
                                secondLayerAntiClockDirection = true;
                            }
                            else
                            {

                                secondLayerClockTime_new = 0;
                                int clockRotation = Random.Range(1, 9);
                                if (clockRotation <= 4)
                                {
                                    secondLayerClockDirection = true;
                                    secondLayerAntiClockDirection = false;
                                }
                                else
                                {
                                    secondLayerClockDirection = false;
                                    secondLayerAntiClockDirection = true;
                                }
                            }
                            // Write Functional Desription Here

                            totalDifficulty = targetSize + layerNumber + secondLayerSize_new + secondlayerSpeed_new + secondLayerClockTime_new;
                            layer_2_totalDifficulty = totalDifficulty;
                            //Debug.Log("Total Difficulty "+ totalDifficulty);
                            // Debug.Log("Current Level Power "+ CurrentLevelPower());
                            // Debug.Log("Next Level Power "+ NextLevelPower());


                            if (totalDifficulty >= CurrentLevelPower() && totalDifficulty < NextLevelPower() && totalDifficulty != layer_1_totalDifficulty)
                            {
                            
                            
                                //Debug.Log("Layer __ 1 total difficulty --------------------------------------------------------" + layer_1_totalDifficulty);
                                diff.circleSize = targetSizeValue_new;
                                //Debug.Log("All New targetSizeValue_new ****************************************" + targetSizeValue_new);

                                diff.numberofLayer = numberLayerValue_new;
                                //Debug.Log("All New numberLayerValue_new ****************************************" + numberLayerValue_new);


                                diff.layerFlexibility = secondLayerClockTime_new;
                                //Debug.Log("All New firstLayerClockTime_new ****************************************" + firstLayerClockTime_new);

                                diff.layerMaxSize = secondLayerSize_new;
                                //Debug.Log("All New firstLayerSize_new ****************************************" + firstLayerSize_new);

                                diff.layerMaxSpeed = secondlayerSpeed_new;
                                //Debug.Log("All New firstlayerSpeed_new ****************************************" + firstlayerSpeed_new);


                                diff.layerclockTime = secondLayerClockTime_new;
                                //Debug.Log("All New firstLayerClockTime_new ****************************************" + firstLayerClockTime_new);
                                //diff.layerClockDirection = newFirstLayerClockDirection;
                                diff.layerTotalDifficulty = totalDifficulty;

                                difficultiesList.Add(diff);
                            
                       
                        
                                
                            }
                        }

                    }
                }
            
        
        int randomListIndex = Random.Range(1, difficultiesList.Count);
        if (randomListIndex == difficultiesList.Count)
        {
            randomListIndex = difficultiesList.Count - 1;
        }
        AssignGenerationValue_Second_Layer_New(randomListIndex);     
    }


    void NewCalculationforLayerThree(int targetSize, int layerNumber)
    {
        Debug.Log("layer ThReE calculation Begin");

        for (int k = 0; k < 3; k++)
        {
            if (k == 0)
            {
                thirdLayerSize_new = layerSize_1;
            }
            else if (k == 1)
            {
                thirdLayerSize_new = layerSize_2;
            }
            else if (k == 2)
            {
                thirdLayerSize_new = maximumLayerSize;
            }
            for (int l = 0; l < 6; l++)
            {
                if (l == 0)
                {
                    thirdlayerSpeed_new = layerSpeed_1;
                }
                else if (l == 1)
                {
                    thirdlayerSpeed_new = layerSpeed_2;
                }
                else if (l == 2)
                {
                    thirdlayerSpeed_new = layerSpeed_3;
                }
                else if (l == 3)
                {
                    thirdlayerSpeed_new = layerSpeed_4;
                }
                else if (l == 4)
                {
                    thirdlayerSpeed_new = layerSpeed_5;
                }
                else if (l == 5)
                {
                    thirdlayerSpeed_new = maximumLayerSpeed;
                }

                for (int m = 0; m < 3; m++)
                {
                    if (m == 0 && runningLevelNumber >= 5)
                    {
                        thirdLayerClockTime_new = clockTime_1 + LayerFlexibility();
                        thirdLayerClockDirection = true;
                        thirdLayerAntiClockDirection = true;

                    }
                    else if (m == 1 && runningLevelNumber >= 6)
                    {
                        thirdlayerSpeed_new = clockTime_2 + LayerFlexibility();
                        thirdLayerClockDirection = true;
                        thirdLayerAntiClockDirection = true;
                    }
                    else if (m == 2 && runningLevelNumber >= 7)
                    {
                        thirdlayerSpeed_new = maximumClockTime + LayerFlexibility();
                        thirdLayerClockDirection = true;
                        thirdLayerAntiClockDirection = true;
                    }
                    else
                    {

                        thirdLayerClockTime_new = 0;
                        int clockRotation = Random.Range(1, 9);
                        if (clockRotation <= 4)
                        {
                            thirdLayerClockDirection = true;
                            thirdLayerAntiClockDirection = false;
                        }
                        else
                        {
                            thirdLayerClockDirection = false;
                            thirdLayerAntiClockDirection = true;
                        }
                    }
                    // Write Functional Desription Here

                    totalDifficulty = targetSize + layerNumber + thirdLayerSize_new + thirdlayerSpeed_new + thirdLayerClockTime_new;
                    //Debug.Log("Total Difficulty "+ totalDifficulty);
                    // Debug.Log("Current Level Power "+ CurrentLevelPower());
                    // Debug.Log("Next Level Power "+ NextLevelPower());


                    if (totalDifficulty >= CurrentLevelPower() && totalDifficulty < NextLevelPower() && totalDifficulty != layer_1_totalDifficulty && totalDifficulty != layer_2_totalDifficulty)
                    {
                        //Debug.Log("All New Difficulty --------------------------------------------------------" + totalDifficulty);
                        diff.circleSize = targetSizeValue_new;
                        //Debug.Log("All New targetSizeValue_new ****************************************" + targetSizeValue_new);

                        diff.numberofLayer = numberLayerValue_new;
                        //Debug.Log("All New numberLayerValue_new ****************************************" + numberLayerValue_new);


                        diff.layerFlexibility = thirdLayerClockTime_new;
                        //Debug.Log("All New firstLayerClockTime_new ****************************************" + firstLayerClockTime_new);

                        diff.layerMaxSize = thirdLayerSize_new;
                        //Debug.Log("All New firstLayerSize_new ****************************************" + firstLayerSize_new);

                        diff.layerMaxSpeed = thirdlayerSpeed_new;
                        //Debug.Log("All New firstlayerSpeed_new ****************************************" + firstlayerSpeed_new);


                        diff.layerclockTime = thirdLayerClockTime_new;
                        //Debug.Log("All New firstLayerClockTime_new ****************************************" + firstLayerClockTime_new);
                        //diff.layerClockDirection = newFirstLayerClockDirection;
                        diff.layerTotalDifficulty = totalDifficulty;

                        difficultiesList.Add(diff);
                    }
                }

            }
        }


        int randomListIndex = Random.Range(1, difficultiesList.Count);
        if (randomListIndex == difficultiesList.Count)
        {
            randomListIndex = difficultiesList.Count - 1;
        }
        AssignGenerationValue_Third_Layer_New(randomListIndex);



    }


    void CalculationforLayerOne(string fromFunctionItComes, int levelEstimatedPower, bool isLayerAbletoFlexible)
    {
        bool isPowerDistributed = true;
        int loop = 0;
        Debug.Log("Level Estimated Power for Layer 1 ^^^^^^^^^^^^^^^^^^^^^^^^<<<<<<<<<<<<<<" + levelEstimatedPower);

        //newNumberofLayer = 1;
        ////Debug.Log("First Layer Max Size >> << >> << " + firstLayerMaxSize);
        while (difficultiesList.Count == 0)
        {
            for (int i = 1; i <= firstLayerMaxSize; i++)
            {
                for (int j = 1; j <= firstLayerMaxSpeed; j++)
                {

                    int clockDirectionTemp = Random.Range(1, 4);
                    int clockTimingTemp = Random.Range(1, 4);
                    newFirstLayerClockDirection = ClockDirectionFirstLayer(clockDirectionTemp);
                    ////Debug.Log(" New First Layer Clock Direction ================= " + newFirstLayerClockDirection);
                    if (firstLayerClockDirection && firstLayerAntiClockDirection)
                    {
                        newFirstLayerClockTime = ClockTiming(clockTimingTemp);
                        ////Debug.Log(" New First Layer Clock Time ================= " + newFirstLayerClockTime);
                    }
                    else
                    {
                        newFirstLayerClockTime = 0;
                    }

                    if (isLayerAbletoFlexible)
                    {
                        int flexValue = Random.Range(1, 8);
                        newFirstLayerFlexible = FlexibilityFirstLayer(flexValue);
                    }
                    else
                    {
                        newFirstLayerFlexible = FlexibilityFirstLayer(1);
                    }
                    newFirstLayerMaxSize = i;
                    ////Debug.Log(" New First Layer Max Size ================= " + newFirstLayerMaxSize);
                    newFirstLayerMaxSpeed = j;
                    ////Debug.Log(" New First Layer Max Speed ================= " + newFirstLayerMaxSpeed);

                    totalDifficulty = newFirstLayerFlexible +
                                      newFirstLayerMaxSize +
                                      newFirstLayerMaxSpeed+ newFirstLayerClockDirection
                                      + newFirstLayerClockTime;

                    Debug.Log("Total Difficulty ********************************************** " + totalDifficulty);
                    ////Debug.Log("New Layer Flexibility " + newFirstLayerFlexible);
                    ////Debug.Log("New firstLayerMaxSize  " + newFirstLayerMaxSize);
                    ////Debug.Log("New firstLayerMaxSpeed  " + newFirstLayerMaxSpeed);
                    ////Debug.Log("New firstLayer Clock Direction  " + newFirstLayerClockDirection);
                    ////Debug.Log("New firstLayer Clock Time  " + newFirstLayerClockTime);

                    //if (totalDifficulty <= runningLevelPower+2 && totalDifficulty>= (powerIncrementPerLevel+2))
                    if (runningLevelNumber == 1)
                    {
                        previousLevelPower = 6;
                    }
                    else
                    {
                        previousLevelPower = levelEstimatedPower - powerIncrementPerLevel;

                    }
                    if (totalDifficulty < levelEstimatedPower + 2 && totalDifficulty > previousLevelPower + 3 && runningLevelNumber > 1)
                    {
                        Debug.Log("Inside Range value _|_|_|_______________________________________________________________  ");
                        diff.layerFlexibility = newFirstLayerFlexible;
                        diff.layerMaxSize = newFirstLayerMaxSize;
                        diff.layerMaxSpeed = newFirstLayerMaxSpeed;
                        diff.layerclockTime = newFirstLayerClockTime;
                        diff.layerClockDirection = newFirstLayerClockDirection;
                        diff.layerTotalDifficulty = totalDifficulty;

                        difficultiesList.Add(diff);
                    }
                    else if (totalDifficulty < levelEstimatedPower + 1 && totalDifficulty > previousLevelPower + 1)
                    {
                        Debug.Log("Inside Exception __________________________________________________________________  ");


                        diff.layerFlexibility = newFirstLayerFlexible;
                        Debug.Log("Inside Exception New Number of layer  ____________________________________ " + newNumberofLayer);
                        diff.layerMaxSize = newFirstLayerMaxSize;
                        diff.layerMaxSpeed = newFirstLayerMaxSpeed;
                        diff.layerclockTime = newFirstLayerClockTime;
                        diff.layerClockDirection = newFirstLayerClockDirection;
                        diff.layerTotalDifficulty = totalDifficulty;

                        difficultiesList.Add(diff);
                    }

                }

            }
            loop++;
            Debug.Log("Loop Value Layer 1 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" + loop);

            if (loop >= 15)
            {
                isPowerDistributed = false;
                Debug.Log("Level Estimated Power ^^^^^^^^^^^^^^^^^^^^^^^^^^^<<<<<<<<<<<<<<<<<<<<<<<<" + levelEstimatedPower);
                break;

            }
        }

        if (isPowerDistributed)
        {
            int randomListIndex = Random.Range(1, difficultiesList.Count);
            ////Debug.Log("Difficulty List ::::::::::::::::::::::::::::::::::::: "+ difficultiesList.Count);
            if (randomListIndex == difficultiesList.Count)
            {
                randomListIndex = difficultiesList.Count - 1;
                ////Debug.Log("Random List Index else %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% "+ randomListIndex );
            }
            else
            {
                ////Debug.Log("Random List Index else %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% " + randomListIndex);
            }
            AssignGenerationValue_First_Layer(randomListIndex);
        }
        else
        {
            /////StageGenValue();
        }

     

    }

    void CalculationforLayerTwo(int levelEstimatedPower, bool isLayerAbletoFlexible)
    {
        int loop = 0;
        Debug.Log("Level Estimated Power for Layer 2 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" + levelEstimatedPower);

        while (difficultiesList.Count == 0)
        {
            for (int i = 1; i <= secondLayerMaxSize; i++)
            {
                for (int j = 1; j <= secondLayerMaxSpeed; j++)
                {

                    int clockDirectionTemp = Random.Range(1, 4);
                    int clockTimingTemp = Random.Range(1, 4);
                    newSecondLayerClockDirection = ClockDirectionSecondLayer(clockDirectionTemp);

                    if (secondLayerClockDirection && secondLayerAntiClockDirection)
                    {
                        newSecondLayerClockTime = ClockTiming(clockTimingTemp);
                        ////Debug.Log(" New First Layer Clock Time ================= " + newFirstLayerClockTime);
                    }
                    if (isLayerAbletoFlexible)
                    {
                        int flexValue = Random.Range(1, 8);
                        newSecondLayerFlexible = FlexibilitySecondLayer(flexValue);
                    }
                    else
                    {
                        newSecondLayerFlexible = FlexibilitySecondLayer(1);
                    }
                    newSecondLayerMaxSize = i;
                    newSecondLayerMaxSpeed = j;
                    totalDifficulty = newSecondLayerFlexible +
                                      newSecondLayerMaxSize +
                                      newSecondLayerMaxSpeed + newSecondLayerClockDirection
                                      + newSecondLayerClockTime;

                    ////Debug.Log("Total Difficulty " + totalDifficulty);
                    ////Debug.Log("Layer Flexibility " + newSecondLayerFlexible);
                    ////Debug.Log("New firstLayerMaxSize  " + newSecondLayerMaxSize);
                    ////Debug.Log("New firstLayerMaxSpeed  " + newSecondLayerMaxSpeed);
                    ////Debug.Log("New firstLayer Clock Direction  " + newSecondLayerClockDirection);
                    ////Debug.Log("New firstLayer Clock Time  " + newSecondLayerClockTime);

                    //if (totalDifficulty <= runningLevelPower+2 && totalDifficulty>= (powerIncrementPerLevel+2))

                    if (totalDifficulty < levelEstimatedPower + 2 && totalDifficulty > previousLevelPower + 3)
                    {
                        diff.layerFlexibility = newSecondLayerFlexible;
                        diff.layerMaxSize = newSecondLayerMaxSize;
                        diff.layerMaxSpeed = newSecondLayerMaxSpeed;
                        diff.layerclockTime = newSecondLayerClockTime;
                        diff.layerClockDirection = newSecondLayerClockDirection;
                        diff.layerTotalDifficulty = totalDifficulty;

                        difficultiesList.Add(diff);
                    }

                }

            }
            loop++;
            Debug.Log("Loop Value Layer 2 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" + loop);

            if (loop >= 15)
            {
                Debug.Log("Level Estimated Power break ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" + levelEstimatedPower);
                break;
            }
        }



        int randomListIndex = Random.Range(1, difficultiesList.Count);
        if (randomListIndex == difficultiesList.Count)
        {
            randomListIndex = difficultiesList.Count - 1;
            ////Debug.Log("Random List Index Secon layer else %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% " + randomListIndex);
        }
        else
        {
            ////Debug.Log("Random List Index Secon layer else %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% " + randomListIndex);
        }

        AssignGenerationValue_Second_Layer(randomListIndex);

    }


    void CalculationforLayerThree(int levelEstimatedPower, bool isLayerAbletoFlexible)
    {
        int loop = 0;
        Debug.Log("Level Estimated Power for Layer 2 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" + levelEstimatedPower);

        while (difficultiesList.Count == 0)
        {
            for (int i = 1; i <= thirdLayerMaxSize; i++)
            {
                for (int j = 1; j <= thirdLayerMaxSpeed; j++)
                {

                    int clockDirectionTemp = Random.Range(1, 4);
                    int clockTimingTemp = Random.Range(1, 4);
                    newThirdLayerClockDirection = ClockDirectionThirdLayer(clockDirectionTemp);

                    if (thirdLayerClockDirection && thirdLayerAntiClockDirection)
                    {
                        newThirdLayerClockTime = ClockTiming(clockTimingTemp);
                        ////Debug.Log(" New First Layer Clock Time ================= " + newFirstLayerClockTime);
                    }
                    if (isLayerAbletoFlexible)
                    {
                        int flexValue = Random.Range(1, 8);
                        newThirdLayerFlexible = FlexibilityThirdLayer(flexValue);
                    }
                    else
                    {
                        newThirdLayerFlexible = FlexibilityThirdLayer(1);
                    }
                    newThirdLayerMaxSize = i;
                    newThirdLayerMaxSpeed = j;
                    totalDifficulty = newThirdLayerFlexible +
                                      newThirdLayerMaxSize +
                                      newThirdLayerMaxSpeed + newThirdLayerClockDirection
                                      + newThirdLayerClockTime;

                    ////Debug.Log("Total Difficulty " + totalDifficulty);
                    ////Debug.Log("Layer Flexibility " + newSecondLayerFlexible);
                    ////Debug.Log("New firstLayerMaxSize  " + newSecondLayerMaxSize);
                    ////Debug.Log("New firstLayerMaxSpeed  " + newSecondLayerMaxSpeed);
                    ////Debug.Log("New firstLayer Clock Direction  " + newSecondLayerClockDirection);
                    ////Debug.Log("New firstLayer Clock Time  " + newSecondLayerClockTime);

                    //if (totalDifficulty <= runningLevelPower+2 && totalDifficulty>= (powerIncrementPerLevel+2))

                    if (totalDifficulty < levelEstimatedPower + 2 && totalDifficulty > previousLevelPower + 3)
                    {
                        diff.layerFlexibility = newThirdLayerFlexible;
                        diff.layerMaxSize = newThirdLayerMaxSize;
                        diff.layerMaxSpeed = newThirdLayerMaxSpeed;
                        diff.layerclockTime = newThirdLayerClockTime;
                        diff.layerClockDirection = newThirdLayerClockDirection;
                        diff.layerTotalDifficulty = totalDifficulty;

                        difficultiesList.Add(diff);
                    }

                }

            }
            loop++;
            Debug.Log("Loop Value Layer 2 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||" + loop);

            if (loop >= 15)
            {
                Debug.Log("Level Estimated Power break ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" + levelEstimatedPower);
                break;
            }
        }



        int randomListIndex = Random.Range(1, difficultiesList.Count);
        if (randomListIndex == difficultiesList.Count)
        {
            randomListIndex = difficultiesList.Count - 1;
            ////Debug.Log("Random List Index Secon layer else %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% " + randomListIndex);
        }
        else
        {
            ////Debug.Log("Random List Index Secon layer else %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% " + randomListIndex);
        }

        AssignGenerationValue_Third_Layer(randomListIndex);

    }


    private void Reset()
    {
        for (int i = 0; i < largeThreeObstacleList.Count; i++)
        {
            largeThreeObstacleList[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < circleList.Count; i++)
        {
            circleList[i].gameObject.SetActive(false);
        }
    }

    public void NewStageGen()
    {
        if (numberofLayer == 3)
        {
            Debug.Log("New Stage Gen Number of Layer --- 3 ");
            LayerThreeCalculation();
        }
        else if (numberofLayer == 2)
        {
            Debug.Log("New Stage Gen Number of Layer --- 2 ");
            LayerTwoCalculation();
        }
        else if (numberofLayer == 1)
        {
            Debug.Log("New Stage Gen Number of Layer --- 1 ");
            LayerOneCalculation();
        }

    }

    void NewStageGenClockTime_First(int indexNumber)
    {
        if (firstLayerclockTime == 1)
        {
            largeThreeObstacleList[indexNumber].waitingTime = 6f;
        }
        else if (firstLayerclockTime == 2)
        {
            largeThreeObstacleList[indexNumber].waitingTime = 3f;
        }
        else if (firstLayerclockTime == 3)
        {
            largeThreeObstacleList[indexNumber].waitingTime = 2f;
        }
    }

    void NewStageGenClockTime_Second(int indexNumber)
    {
        if (secondLayerclockTime == 1)
        {
            largeThreeObstacleList[indexNumber].waitingTime = 6f;
        }
        else if (secondLayerclockTime == 2)
        {
            largeThreeObstacleList[indexNumber].waitingTime = 3f;
        }
        else if (secondLayerclockTime == 3)
        {
            largeThreeObstacleList[indexNumber].waitingTime = 2f;
        }
    }

    void NewStageGenClockTime_Third(int indexNumber)
    {
        if (thirdLayerclockTime == 1)
        {
            largeThreeObstacleList[indexNumber].waitingTime = 6f;
        }
        else if (thirdLayerclockTime == 2)
        {
            largeThreeObstacleList[indexNumber].waitingTime = 3f;
        }
        else if (thirdLayerclockTime == 3)
        {
            largeThreeObstacleList[indexNumber].waitingTime = 2f;
        }
    }

    void NewStageGenClockDirection_First(int indexNumber)
    {
        if (firstLayerClockDirection && !firstLayerAntiClockDirection)
        {
            largeThreeObstacleList[indexNumber].clockDirection = true;
            largeThreeObstacleList[indexNumber].counterClockDirection = false;

        }
        else if (firstLayerAntiClockDirection && !firstLayerClockDirection)
        {
            largeThreeObstacleList[indexNumber].clockDirection = false;
            largeThreeObstacleList[indexNumber].counterClockDirection = true;
        }
        else if (firstLayerClockDirection && firstLayerAntiClockDirection)
        {
            largeThreeObstacleList[indexNumber].clockDirection = true;
            largeThreeObstacleList[indexNumber].counterClockDirection = true;
            NewStageGenClockTime_First(indexNumber);
        }

    }

    void NewStageGenClockDirection_Second(int indexNumber)
    {
        if (secondLayerClockDirection && !secondLayerAntiClockDirection)
        {
            largeThreeObstacleList[indexNumber].clockDirection = true;
            largeThreeObstacleList[indexNumber].counterClockDirection = false;

        }
        else if (secondLayerAntiClockDirection && !secondLayerClockDirection)
        {
            largeThreeObstacleList[indexNumber].clockDirection = false;
            largeThreeObstacleList[indexNumber].counterClockDirection = true;
        }
        else if (secondLayerClockDirection && secondLayerAntiClockDirection)
        {
            largeThreeObstacleList[indexNumber].clockDirection = true;
            largeThreeObstacleList[indexNumber].counterClockDirection = true;
            NewStageGenClockTime_Second(indexNumber);
        }

    }


    void NewStageGenClockDirection_Third(int indexNumber)
    {
        if (thirdLayerClockDirection && !thirdLayerAntiClockDirection)
        {
            largeThreeObstacleList[indexNumber].clockDirection = true;
            largeThreeObstacleList[indexNumber].counterClockDirection = false;

        }
        else if (thirdLayerAntiClockDirection && !thirdLayerClockDirection)
        {
            largeThreeObstacleList[indexNumber].clockDirection = false;
            largeThreeObstacleList[indexNumber].counterClockDirection = true;
        }
        else if (thirdLayerClockDirection && thirdLayerAntiClockDirection)
        {
            largeThreeObstacleList[indexNumber].clockDirection = true;
            largeThreeObstacleList[indexNumber].counterClockDirection = true;
            NewStageGenClockTime_Third(indexNumber);
        }

    }

    void NewStageSpeedGenFirstLayer(int indexNumber)
    {
        if (firstLayerSpeed == 1)
        {
            Debug.Log("First Layer Small Speed Gen 1 " +indexNumber);
            largeThreeObstacleList[indexNumber].speed = 130f;
            NewStageGenClockDirection_First(indexNumber);

        }
        else if (firstLayerSpeed == 2)
        {
            Debug.Log("First Layer Small Speed Gen 2 " + indexNumber);
            largeThreeObstacleList[indexNumber].speed = 160f;
            NewStageGenClockDirection_First(indexNumber);
        }
        else if (firstLayerSpeed == 3)
        {
            Debug.Log("First Layer Small Speed Gen 3 " + indexNumber);
            largeThreeObstacleList[indexNumber].speed = 190f;
            NewStageGenClockDirection_First(indexNumber);
        }
        else if (firstLayerSpeed == 4)
        {
            Debug.Log("First Layer Small Speed Gen 4");
            largeThreeObstacleList[indexNumber].speed = 240f;
            NewStageGenClockDirection_First(indexNumber);
        }
        else if (firstLayerSpeed == 5)
        {
            largeThreeObstacleList[indexNumber].speed = 280f;
            NewStageGenClockDirection_First(indexNumber);
        }
        else if (firstLayerSpeed == 6)
        {
            largeThreeObstacleList[indexNumber].speed = 350f;
            NewStageGenClockDirection_First(indexNumber);
        }
    }




    void NewStageSpeedGenSecondLayer(int indexNumber)
    {
        if (secondLayerSpeed == 1)
        {
            largeThreeObstacleList[indexNumber].speed = 130f;
            NewStageGenClockDirection_Second(indexNumber);
        }
        else if (secondLayerSpeed == 2)
        {
            largeThreeObstacleList[indexNumber].speed = 160f;
            NewStageGenClockDirection_Second(indexNumber);
        }
        else if (secondLayerSpeed == 3)
        {
            largeThreeObstacleList[indexNumber].speed = 190f;
            NewStageGenClockDirection_Second(indexNumber);
        }
        else if (secondLayerSpeed == 4)
        {
            largeThreeObstacleList[indexNumber].speed = 240f;
            NewStageGenClockDirection_Second(indexNumber);
        }
        else if (secondLayerSpeed == 5)
        {
            largeThreeObstacleList[indexNumber].speed = 280f;
            NewStageGenClockDirection_Second(indexNumber);
        }
        else if (secondLayerSpeed == 6)
        {
            largeThreeObstacleList[indexNumber].speed = 350f;
            NewStageGenClockDirection_Second(indexNumber);
        }
    }


    void NewStageSpeedGenThirdLayer(int indexNumber)
    {
        if (thirdLayerSpeed == 1)
        {
            largeThreeObstacleList[indexNumber].speed = 130f;
            NewStageGenClockDirection_Third(indexNumber);
        }
        else if (thirdLayerSpeed == 2)
        {
            largeThreeObstacleList[indexNumber].speed = 160f;
            NewStageGenClockDirection_Third(indexNumber);
        }
        else if (thirdLayerSpeed == 3)
        {
            largeThreeObstacleList[indexNumber].speed = 190f;
            NewStageGenClockDirection_Third(indexNumber);
        }
        else if (thirdLayerSpeed == 4)
        {
            largeThreeObstacleList[indexNumber].speed = 240f;
            NewStageGenClockDirection_Third(indexNumber);
        }
        else if (thirdLayerSpeed == 5)
        {
            largeThreeObstacleList[indexNumber].speed = 280f;
            NewStageGenClockDirection_Third(indexNumber);
        }
        else if (thirdLayerSpeed == 6)
        {
            largeThreeObstacleList[indexNumber].speed = 350f;
            NewStageGenClockDirection_Third(indexNumber);
        }
    }


    void NewStageSpeedGenFourthLayer(int indexNumber)
    {
        if (firstLayerSpeed == 1)
        {
            largeThreeObstacleList[indexNumber].speed = 150f - 10f;
            NewStageGenClockDirection_First(indexNumber);
        }
        else if (firstLayerSpeed == 2)
        {
            largeThreeObstacleList[indexNumber].speed = 170f - 10f;
            NewStageGenClockDirection_First(indexNumber);
        }
        else if (firstLayerSpeed == 3)
        {
            largeThreeObstacleList[indexNumber].speed = 190f - 50f;
            NewStageGenClockDirection_First(indexNumber);
        }
        else if (firstLayerSpeed == 4)
        {
            largeThreeObstacleList[indexNumber].speed = 240f - 60f;
            NewStageGenClockDirection_First(indexNumber);
        }
        else if (firstLayerSpeed == 5)
        {
            largeThreeObstacleList[indexNumber].speed = 280f - 60f;
            NewStageGenClockDirection_First(indexNumber);
        }
        else if (firstLayerSpeed == 6)
        {
            largeThreeObstacleList[indexNumber].speed = 350f - 70f;
            NewStageGenClockDirection_First(indexNumber);
        }
    }
    void NewStageSpeedGenFifthLayer(int indexNumber)
    {
        if (secondLayerSpeed == 1)
        {
            largeThreeObstacleList[indexNumber].speed = 130f - 20f;
            NewStageGenClockDirection_Second(indexNumber);
        }
        else if (secondLayerSpeed == 2)
        {
            largeThreeObstacleList[indexNumber].speed = 160f - 30f;
            NewStageGenClockDirection_Second(indexNumber);
        }
        else if (secondLayerSpeed == 3)
        {
            largeThreeObstacleList[indexNumber].speed = 190f - 40f;
            NewStageGenClockDirection_Second(indexNumber);
        }
        else if (secondLayerSpeed == 4)
        {
            largeThreeObstacleList[indexNumber].speed = 240f - 60f;
            NewStageGenClockDirection_Second(indexNumber);
        }
        else if (secondLayerSpeed == 5)
        {
            largeThreeObstacleList[indexNumber].speed = 280f - 70f;
            NewStageGenClockDirection_Second(indexNumber);
        }
        else if (secondLayerSpeed == 6)
        {
            largeThreeObstacleList[indexNumber].speed = 350f - 60f;
            NewStageGenClockDirection_Second(indexNumber);
        }
    }


    void NewStageSpeedGenSixthLayer(int indexNumber)
    {
        if (thirdLayerSpeed == 1)
        {
            largeThreeObstacleList[indexNumber].speed = 130f - 20f;
            NewStageGenClockDirection_First(indexNumber);
        }
        else if (thirdLayerSpeed == 2)
        {
            largeThreeObstacleList[indexNumber].speed = 160f - 10f;
            NewStageGenClockDirection_First(indexNumber);
        }
        else if (thirdLayerSpeed == 3)
        {
            largeThreeObstacleList[indexNumber].speed = 190f - 30f;
            NewStageGenClockDirection_First(indexNumber);
        }
        else if (thirdLayerSpeed == 4)
        {
            largeThreeObstacleList[indexNumber].speed = 240f - 20f;
            NewStageGenClockDirection_First(indexNumber);
        }
        else if (thirdLayerSpeed == 5)
        {
            largeThreeObstacleList[indexNumber].speed = 280f - 40f;
            NewStageGenClockDirection_First(indexNumber);
        }
        else if (thirdLayerSpeed == 6)
        {
            largeThreeObstacleList[indexNumber].speed = 350f - 50f;
            NewStageGenClockDirection_First(indexNumber);
        }
    }

    void LayerThreeCalculation()
    {
        if (firstLayerSize == 3)
        {
            if (firstLayerFlexible)
            {
                //First Layer
                largeThreeObstacleList[3].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(3);


                largeThreeObstacleList[4].gameObject.SetActive(true);
                NewStageSpeedGenFourthLayer(4);

                ////Second Layer
                //largeThreeObstacleList[5].gameObject.SetActive(true);
                //NewStageSpeedGenFourthLayer(5);

                //largeThreeObstacleList[6].gameObject.SetActive(true);
                //NewStageSpeedGenFifthLayer(6);

                ////Third Layer
                //largeThreeObstacleList[7].gameObject.SetActive(true);
                //NewStageSpeedGenSecondLayer(7);

                //largeThreeObstacleList[8].gameObject.SetActive(true);
                //NewStageSpeedGenSixthLayer(8);

            }
            //else if ()
            //{

            //}

            else
            {
                //First Layer
                largeThreeObstacleList[0].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(0);

                //Second Layer
                //largeThreeObstacleList[1].gameObject.SetActive(true);
                //NewStageSpeedGenSecondLayer(1);

                //Third layer
                //largeThreeObstacleList[2].gameObject.SetActive(true);
                //NewStageSpeedGenThirdLayer(2);
            }

        }
        if (firstLayerSize == 2)
        {
            if (firstLayerFlexible)
            {
                //First Layer
                largeThreeObstacleList[12].gameObject.SetActive(true);
                NewStageSpeedGenFourthLayer(12);

                largeThreeObstacleList[13].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(13);

                //Second Layer
                // largeThreeObstacleList[14].gameObject.SetActive(true);
                // NewStageSpeedGenFourthLayer(14);

                // largeThreeObstacleList[15].gameObject.SetActive(true);
                // NewStageSpeedGenFifthLayer(15);

                //Third Layer
                //largeThreeObstacleList[16].gameObject.SetActive(true);
                // NewStageSpeedGenSecondLayer(16);

                //largeThreeObstacleList[17].gameObject.SetActive(true);
                // NewStageSpeedGenSixthLayer(17);

            }
            else
            {
                //First Layer
                largeThreeObstacleList[9].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(9);

                //Second Layer
                //largeThreeObstacleList[10].gameObject.SetActive(true);
                //NewStageSpeedGenSecondLayer(10);

                //Third Layer
                //largeThreeObstacleList[11].gameObject.SetActive(true);
                //NewStageSpeedGenThirdLayer(11);
            }
        }
        if (firstLayerSize == 1)
        {
            if (firstLayerFlexible)
            {
                //First Layer
                largeThreeObstacleList[12].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(12);

                largeThreeObstacleList[13].gameObject.SetActive(true);
                NewStageSpeedGenFourthLayer(13);

                ////Second Layer
                //largeThreeObstacleList[14].gameObject.SetActive(true);
                //NewStageSpeedGenFourthLayer(14);

                //largeThreeObstacleList[15].gameObject.SetActive(true);
                //NewStageSpeedGenFifthLayer(15);

                ////Third layer
                //largeThreeObstacleList[16].gameObject.SetActive(true);
                //NewStageSpeedGenSecondLayer(16);

                //largeThreeObstacleList[17].gameObject.SetActive(true);
                //NewStageSpeedGenSixthLayer(17);

            }
            else
            {
                //First Layer
                largeThreeObstacleList[9].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(9);

                //Second Layer
                //largeThreeObstacleList[10].gameObject.SetActive(true);
                //NewStageSpeedGenSecondLayer(10);

                //Thirdlayer
                //largeThreeObstacleList[11].gameObject.SetActive(true);
                //NewStageSpeedGenThirdLayer(11);
            }
        }






        if (secondLayerSize == 3)
        {
            if (secondLayerFlexible)
            {

                //Second Layer
                largeThreeObstacleList[5].gameObject.SetActive(true);
                NewStageSpeedGenSecondLayer(5);

                largeThreeObstacleList[6].gameObject.SetActive(true);
                NewStageSpeedGenFifthLayer(6);



            }


            else
            {


                //Second Layer
                largeThreeObstacleList[1].gameObject.SetActive(true);
                NewStageSpeedGenSecondLayer(1);


            }

        }
        if (secondLayerSize == 2)
        {
            if (secondLayerFlexible)
            {


                //Second Layer
                largeThreeObstacleList[14].gameObject.SetActive(true);
                NewStageSpeedGenFifthLayer(14);

                largeThreeObstacleList[15].gameObject.SetActive(true);
                NewStageSpeedGenSecondLayer(15);

                //Third Layer
                //largeThreeObstacleList[16].gameObject.SetActive(true);
                // NewStageSpeedGenSecondLayer(16);

                //largeThreeObstacleList[17].gameObject.SetActive(true);
                // NewStageSpeedGenSixthLayer(17);

            }
            else
            {


                //Second Layer
                largeThreeObstacleList[10].gameObject.SetActive(true);
                NewStageSpeedGenSecondLayer(10);

                //Third Layer
                //largeThreeObstacleList[11].gameObject.SetActive(true);
                //NewStageSpeedGenThirdLayer(11);
            }
        }
        if (secondLayerSize == 1)
        {
            if (secondLayerFlexible)
            {

                ////Second Layer
                largeThreeObstacleList[14].gameObject.SetActive(true);
                NewStageSpeedGenSecondLayer(14);

                largeThreeObstacleList[15].gameObject.SetActive(true);
                NewStageSpeedGenFifthLayer(15);



            }
            else
            {


                //Second Layer
                largeThreeObstacleList[10].gameObject.SetActive(true);
                NewStageSpeedGenSecondLayer(10);


            }
        }






        if (thirdLayerSize == 3)
        {
            if (thirdLayerFlexible)
            {
                ////Third Layer
                largeThreeObstacleList[7].gameObject.SetActive(true);
                NewStageSpeedGenThirdLayer(7);

                largeThreeObstacleList[8].gameObject.SetActive(true);
                NewStageSpeedGenSixthLayer(8);

            }


            else
            {
                //Third layer
                largeThreeObstacleList[2].gameObject.SetActive(true);
                NewStageSpeedGenThirdLayer(2);
            }

        }
        if (thirdLayerSize == 2)
        {
            if (thirdLayerFlexible)
            {

                //Third Layer
                largeThreeObstacleList[16].gameObject.SetActive(true);
                NewStageSpeedGenSixthLayer(16);

                largeThreeObstacleList[17].gameObject.SetActive(true);
                NewStageSpeedGenThirdLayer(17);

            }
            else
            {

                //Third Layer
                largeThreeObstacleList[11].gameObject.SetActive(true);
                NewStageSpeedGenThirdLayer(11);
            }
        }
        if (thirdLayerSize == 1)
        {
            if (thirdLayerFlexible)
            {
                ////Third layer
                largeThreeObstacleList[16].gameObject.SetActive(true);
                NewStageSpeedGenThirdLayer(16);

                largeThreeObstacleList[17].gameObject.SetActive(true);
                NewStageSpeedGenSixthLayer(17);

            }
            else
            {
                //Thirdlayer
                largeThreeObstacleList[11].gameObject.SetActive(true);
                NewStageSpeedGenThirdLayer(11);
            }
        }
    }

    void LayerTwoCalculation()
    {
        if (firstLayerSize == 3)
        {
            if (firstLayerFlexible)
            {
                //First Layer
                largeThreeObstacleList[3].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(3);

                largeThreeObstacleList[4].gameObject.SetActive(true);
                NewStageSpeedGenFourthLayer(4);

                //Second Layer
                //largeThreeObstacleList[5].gameObject.SetActive(true);
                //NewStageSpeedGenFourthLayer(5);

                //largeThreeObstacleList[6].gameObject.SetActive(true);
                //NewStageSpeedGenFifthLayer(6);


            }
            else
            {
                //First Layer
                largeThreeObstacleList[0].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(0);
               
                //Second Layer
                //largeThreeObstacleList[1].gameObject.SetActive(true);
                //NewStageSpeedGenSecondLayer(1);

            }

        }



        if (firstLayerSize == 2)
        {
            if (firstLayerFlexible)
            {
                //First Layer
                largeThreeObstacleList[12].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(12);

                largeThreeObstacleList[13].gameObject.SetActive(true);
                NewStageSpeedGenFourthLayer(13);

                //Second Layer
                //largeThreeObstacleList[16].gameObject.SetActive(true);
                //NewStageSpeedGenFourthLayer(16);

                //largeThreeObstacleList[17].gameObject.SetActive(true);
                //NewStageSpeedGenSixthLayer(17);

            }
            else
            {
                //First Layer
                largeThreeObstacleList[9].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(9);

                //Second Layer
                //largeThreeObstacleList[10].gameObject.SetActive(true);
                //NewStageSpeedGenSecondLayer(10);

            }
        }



        if (firstLayerSize == 1)
        {
            if (firstLayerFlexible)
            {
                //First Layer
                largeThreeObstacleList[18].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(18);

                largeThreeObstacleList[19].gameObject.SetActive(true);
                NewStageSpeedGenFourthLayer(19);

                //Second Layer
                //largeThreeObstacleList[20].gameObject.SetActive(true);
                //NewStageSpeedGenFourthLayer(20);

                //largeThreeObstacleList[21].gameObject.SetActive(true);
                //NewStageSpeedGenFifthLayer(21);


            }
            else
            {
                //First Layer
                largeThreeObstacleList[22].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(22);

                //Second Layer
                //largeThreeObstacleList[10].gameObject.SetActive(true);
                //NewStageSpeedGenSecondLayer(10);

            }
        }





        if (secondLayerSize == 3)
        {
            if (secondLayerFlexible)
            {
                //First Layer
                //largeThreeObstacleList[3].gameObject.SetActive(true);
                //NewStageSpeedGenFirstLayer(3);

                //largeThreeObstacleList[4].gameObject.SetActive(true);
                //NewStageSpeedGenSecondLayer(4);

                //Second Layer
                largeThreeObstacleList[5].gameObject.SetActive(true);
                NewStageSpeedGenSecondLayer(5);


                largeThreeObstacleList[6].gameObject.SetActive(true);
                NewStageSpeedGenFifthLayer(6);


            }
            else
            {
                //First Layer
                //largeThreeObstacleList[0].gameObject.SetActive(true);
                //NewStageSpeedGenFirstLayer(0);

                //Second Layer
                largeThreeObstacleList[1].gameObject.SetActive(true);
                NewStageSpeedGenSecondLayer(1);

            }

        }



        if (secondLayerSize == 2)
        {
            if (secondLayerFlexible)
            {
                //First Layer
                //largeThreeObstacleList[12].gameObject.SetActive(true);
                //NewStageSpeedGenFirstLayer(12);

                //largeThreeObstacleList[13].gameObject.SetActive(true);
                //NewStageSpeedGenSecondLayer(13);

                //Second Layer
                largeThreeObstacleList[16].gameObject.SetActive(true);
                NewStageSpeedGenSecondLayer(16);

                largeThreeObstacleList[17].gameObject.SetActive(true);
                NewStageSpeedGenFifthLayer(17);

            }
            else
            {
                //First Layer
                //largeThreeObstacleList[9].gameObject.SetActive(true);
                //NewStageSpeedGenFirstLayer(9);

                //Second Layer
                largeThreeObstacleList[10].gameObject.SetActive(true);
                NewStageSpeedGenSecondLayer(10);

            }
        }



        if (secondLayerSize == 1)
        {
            if (secondLayerFlexible)
            {
                //First Layer
                //largeThreeObstacleList[18].gameObject.SetActive(true);
                //NewStageSpeedGenFirstLayer(18);

                //largeThreeObstacleList[19].gameObject.SetActive(true);
                //NewStageSpeedGenSecondLayer(19);

                //Second Layer
                largeThreeObstacleList[20].gameObject.SetActive(true);
                NewStageSpeedGenFifthLayer(20);

                largeThreeObstacleList[21].gameObject.SetActive(true);
                NewStageSpeedGenSecondLayer(21);


            }
            else
            {
                //First Layer
                //largeThreeObstacleList[9].gameObject.SetActive(true);
                //NewStageSpeedGenFirstLayer(9);

                //Second Layer
                largeThreeObstacleList[23].gameObject.SetActive(true);
                NewStageSpeedGenSecondLayer(23);

            }
        }
    }




    void LayerOneCalculation()
    {
        if (firstLayerSize == 3)
        {
            
            if (runningLevelNumber == 2)
            {
                largeThreeObstacleList[0].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(0);
            }
            else
            {
                largeThreeObstacleList[3].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(3);
            }

        }
        else if (firstLayerSize == 2)
        {
            if (runningLevelNumber == 2)
            {
                largeThreeObstacleList[9].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(9);
            }
            else
            {
                largeThreeObstacleList[12].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(12);
            }

        }
        else if (firstLayerSize == 1)
        {
            if (runningLevelNumber == 2)
            {
                largeThreeObstacleList[23].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(23);
            }
            else
            {
                largeThreeObstacleList[18].gameObject.SetActive(true);
                NewStageSpeedGenFirstLayer(18);
            }
        }
    }
}
[System.Serializable]
public struct Difficulty
{
    public int circleSize;
    public int numberofLayer;
    public int layerMaxSize;
    public int layerMaxSpeed;
    public int layerClockDirection;
    public int layerclockTime;
    public int layerTotalDifficulty;
    public int layerFlexibility;
}
