﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageGenController : MonoBehaviour {

    public List<ObstacleController> obstacle;
    [Range(1,10)]
    public int obstacleDifficulty;
    [Range(1, 6)]
    public int speedDifficulty;


    public static StageGenController instance;
    public float allSpeed;
     void Awake()
    {
        instance = this;
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void StageObsGen()
    {
        Debug.Log("Its Called");
        if (obstacleDifficulty == 1)
        {
            obstacle[0].gameObject.SetActive(true);
        }
        else if (obstacleDifficulty == 2)
        {
            obstacle[1].gameObject.SetActive(true);
        }
        else if (obstacleDifficulty == 3)
        {
            obstacle[2].gameObject.SetActive(true);
        }
        else if (obstacleDifficulty == 4)
        {
            obstacle[3].gameObject.SetActive(true);
        }
        else if (obstacleDifficulty == 5)
        {
            obstacle[4].gameObject.SetActive(true);
        }
        else if (obstacleDifficulty == 6)
        {
            obstacle[1].gameObject.SetActive(true);
            obstacle[5].gameObject.SetActive(true);
        }
        else if (obstacleDifficulty == 7)
        {
            obstacle[3].gameObject.SetActive(true);
            obstacle[5].gameObject.SetActive(true);
        }
        else if (obstacleDifficulty == 8)
        {
            obstacle[2].gameObject.SetActive(true);
            obstacle[3].gameObject.SetActive(true);
        }
        else if (obstacleDifficulty == 9)
        {
            obstacle[0].gameObject.SetActive(true);
            obstacle[5].gameObject.SetActive(true);
            obstacle[6].gameObject.SetActive(true);
            //Small flexible
        }
        else if (obstacleDifficulty == 100)
        {
            obstacle[0].gameObject.SetActive(true);
            obstacle[4].gameObject.SetActive(true);
        }
        else if (obstacleDifficulty == 11)
        {
            obstacle[2].gameObject.SetActive(true);
            obstacle[4].gameObject.SetActive(true);
        }
    }
   public void StageSpeedGen()
    {
        if (speedDifficulty == 1)
        {
            //ObstacleController.instance.speed = 130f;
        }
        else if (speedDifficulty == 2)
        {
            //ObstacleController.instance.speed = 160f;
        }
        else if (speedDifficulty == 3)
        {
            //ObstacleController.instance.speed = 190f;
        }
        else if (speedDifficulty == 4)
        {
            //ObstacleController.instance.speed = 240f;
        }
        else if (speedDifficulty == 5)
        {
            //ObstacleController.instance.speed = 280f;
            allSpeed = 280f;
        }
        else if (speedDifficulty == 6)
        {
            //ObstacleController.instance.speed = 350f;
        }
    }
}
