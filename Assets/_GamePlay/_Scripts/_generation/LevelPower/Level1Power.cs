﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level1Power  {

   static List<int> level_1_Power = new List<int>();
   
	public static List<int> Level_1_PowerDistribution(int value)
    {
        if (value == 1)
        {
            
            if (Random.Range(1, 9) < 5)
            {
                Debug.Log("L1 Total Power 50 if ");
                level_1_Power.Add(3);
                level_1_Power.Add(1);
                level_1_Power.Add(1);
                level_1_Power.Add(1);
                return level_1_Power;
            }
            else
            {
                Debug.Log("L1 Total Power 50 else ");
                level_1_Power.Add(2);
                level_1_Power.Add(1);
                level_1_Power.Add(2);
                level_1_Power.Add(2);
                return level_1_Power;
            }
        }
        else if (value==2)
        {
            Debug.Log("L1 Total Power 60 ");
            //Total Power 60
            level_1_Power.Add(3);
            level_1_Power.Add(1);
            level_1_Power.Add(2);
            level_1_Power.Add(1);
            return level_1_Power;
        }
        else if (value == 3)
        {
            Debug.Log("L1 Total Power 65 ");
            //Total Power 65
            level_1_Power.Add(3);
            level_1_Power.Add(1);
            level_1_Power.Add(2);
            level_1_Power.Add(1);
            return level_1_Power;
        }
        else if (value == 4)
        {
            Debug.Log("L1 Total Power 70 ");
            //Total Power 70
            level_1_Power.Add(1);
            level_1_Power.Add(1);
            level_1_Power.Add(3);
            level_1_Power.Add(1);
            return level_1_Power;
        }
        else if (value == 5)
        {
            Debug.Log("L1 Total Power 75 ");
            //Total Power 75
            level_1_Power.Add(3);
            level_1_Power.Add(1);
            level_1_Power.Add(1);
            level_1_Power.Add(3);
            return level_1_Power;
        }
        else
        {
            return level_1_Power;
        }
        
    }
    public static List<int> Level_2_PowerDistribution(int value)
    {
        if (value == 1)
        {
            //Total Power 50
            if (Random.Range(1, 9) < 5)
            {

                level_1_Power.Add(3);
                level_1_Power.Add(1);
                level_1_Power.Add(1);
                level_1_Power.Add(1);
                return level_1_Power;
            }
            else
            {
                level_1_Power.Add(2);
                level_1_Power.Add(1);
                level_1_Power.Add(2);
                level_1_Power.Add(2);
                return level_1_Power;
            }
        }
        else if (value == 2)
        {
            //Total Power 60
            level_1_Power.Add(3);
            level_1_Power.Add(1);
            level_1_Power.Add(2);
            level_1_Power.Add(1);
            return level_1_Power;
        }
        else if (value == 3)
        {
            //Total Power 65
            level_1_Power.Add(3);
            level_1_Power.Add(1);
            level_1_Power.Add(2);
            level_1_Power.Add(1);
            return level_1_Power;
        }
        else if (value == 4)
        {
            //Total Power 70
            level_1_Power.Add(1);
            level_1_Power.Add(1);
            level_1_Power.Add(3);
            level_1_Power.Add(1);
            return level_1_Power;
        }
        else if (value == 5)
        {
            //Total Power 75
            level_1_Power.Add(3);
            level_1_Power.Add(1);
            level_1_Power.Add(1);
            level_1_Power.Add(3);
            return level_1_Power;
        }
        else
        {
            return level_1_Power;
        }

    }
}
