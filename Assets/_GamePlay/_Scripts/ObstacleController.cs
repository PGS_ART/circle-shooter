﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;
using UnityEngine.SceneManagement;

public class ObstacleController : MonoBehaviour {

    //public float speed;
    public float speed;
    public static ObstacleController instance;
     int tempTime;
    public float waitingTime;
    public bool clockDirection;
    public bool counterClockDirection;
    //dibas
    public Animator endmenuanim;
    
    public Animator OtherAnim;
    public Animator FishAnim;
    public ParticleSystem explode;

    public ParticleSystem[] DeathPar;

    // Update is called once per frame
    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        //speed = StageGenController.instance.allSpeed;
        //Debug.Log("Rotation Speed Check ____ " + speed);
        StartCoroutine(SpeedClockingCoroutine());
    }

    void Update () {
        
        if (tempTime==6)
        {
            transform.Rotate(0f, 0f, (-1)*speed * Time.deltaTime);
           
        }
        else if(tempTime==3)
        {
            transform.Rotate(0f, 0f, speed * Time.deltaTime);

        }
            
    }

    

    IEnumerator SpeedClockingCoroutine()
    {
       
            if (clockDirection && !counterClockDirection)
            {
                tempTime = 3;
            }
            else if (counterClockDirection && !clockDirection)
            {
                tempTime = 6;
            }
            else if (clockDirection && counterClockDirection)
            {
            while (true)
            {
                tempTime = 3;
                yield return new WaitForSeconds(waitingTime);
                tempTime = 6;
                yield return new WaitForSeconds(waitingTime);
            }
                
            }
           
           
        
    }

        void OnTriggerEnter2D(Collider2D other)
    {
        explode.Play();
            //StartCoroutine(GameOverCoroutine());
            StartCoroutine(DeathCoroutine());



    }
    IEnumerator GameOverCoroutine()
    {
        Debug.Log("Game Over routine obstacle");
        //Handheld.Vibrate();
        //Vibration.Vibrate(200);
        //AudioController.Instance.Play(AudioController.Instance.ELECTRIC);
        

        //LevelController.instance.setinput(false);
        //LightingController.instance.All = false;

        OtherAnim.Play("ObjectsExit", -1, 0);
        FishAnim.Play("FishDeadAnimation", -1, 0);
        
        yield return new WaitForSeconds(1f);
        AudioController.Instance.Play(AudioController.Instance.EndSad);
        endmenuanim.Play("EndPanel", -1, 0);
        //endanim.Play("EndPanelExit", -1, 0);


        //SceneManager.LoadScene(0);
        //Debug.Log("Game Over");

    }
    IEnumerator DeathCoroutine()
    {
        Debug.Log("Death routine obstacle");
        //Handheld.Vibrate();
        LevelController.instance.setinput(false);
        LightingController.instance.All = false;
        CameraShaker.Instance.ShakeOnce(2f, 2f, .1f, 3f);
        Vibration.Vibrate(300);
        AudioController.Instance.Play(AudioController.Instance.ObsHit);
        AudioController.Instance.Stop(AudioController.Instance.GAME_PLAY);
        
        foreach (ParticleSystem i in DeathPar)
            i.Play();
        FishAnim.Play("FishElectDeath", -1, 0);
        yield return new WaitForSeconds(2f);
        StartCoroutine(GameOverCoroutine());
    }
}
