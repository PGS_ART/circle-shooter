﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour {
    public Animator firstBrokenCircleAnim;
    public Animator SecondBrokenCircleAnim;
    public GameObject TaptoShoot;
    public ParticleSystem BubbleTran;

    //public GameObject firstFiveStage;
    //public List<GameObject> firstFivecircleList;
    //public List<GameObject> firstFiveobstacleList;

    //public GameObject secondFiveStage;
    //public List<GameObject> secondFivecircleList;
    //public List<GameObject> secondFiveobstacleList;

    public static LevelController instance;
    //public bool isNormalStage=true;
    //public int stageVariation;

    //Dibas
    private static bool isInputEnable;
    public void setinput(bool a)
    {
        isInputEnable = a;
    }
    public bool getinput()
    {
        return isInputEnable;
    }

    // Use this for initialization
    void Awake()
    {
        instance = this;
    }
    void Start () {

        isInputEnable = false;
        TaptoShoot.SetActive(false);
        BubbleTran.Play();
        AudioController.Instance.Play(AudioController.Instance.BUBBLETRAN);
        //int result = ((9 * 33)/100f);
        //Debug.Log("ResUlT <>><><><><><>>><><><>< +++ " +result);
        PlayerPrefs.SetInt("StageNumber", 0);
        //StageGeneration();

    }

    //public void ResetActivity()
    //{
    //    int tempObstacleList = firstFiveobstacleList.Count;
    //    for (int i = 0; i < tempObstacleList; i++)
    //    {
    //        firstFiveobstacleList[i].SetActive(false);
    //    }
    //    int tempCicrleList = firstFivecircleList.Count;

    //    for (int i = 0; i < tempCicrleList; i++)
    //    {
    //        firstFivecircleList[i].SetActive(false);
    //    }
    //}
    public void StageGeneration()
    {
       // ResetActivity();
        //Debug.Log("Stage Gen");
        //int stageNumber = PlayerPrefs.GetInt("StageNumber");
        //if ( stageNumber >= 0 &&  stageNumber <= stageVariation)
        //{

            //StageGenController.instance.StageObsGen();
            //StageGenController.instance.StageSpeedGen();

            //Debug.Log("Stage Gen inside if <><><><><>");
            //Debug.Log("Stage Number ><>"+ stageNumber);
            //firstFiveStage.SetActive(true);
            //int randomTemp = Random.Range(0,2);
            //if (randomTemp == 2)
            //{
            //    firstFivecircleList[randomTemp-1].SetActive(true);
            //}
            //else
            //{
            //    Debug.Log("Rendom Temp ^^^^^ "+randomTemp);
            //    firstFivecircleList[randomTemp].SetActive(true);
            //}

            //int randomTempAgain = Random.Range(0, 3);
            //if (randomTempAgain == 3)
            //{
            //    firstFiveobstacleList[randomTempAgain-1].SetActive(true);
            //}
            //else
            //{
            //    firstFiveobstacleList[randomTempAgain].SetActive(true);
            //}
            

            //if (stageNumber>=3 && stageNumber <= 4)
            //{
            //    ObstacleController.instance.speed = 145f;
            //}
            //else if (stageNumber ==5)
            //{
            //    ObstacleController.instance.speed = 170f;
            //}
            //else
            //{
            //    ObstacleController.instance.speed = 220f;
            //}
            firstBrokenCircleAnim.SetTrigger("Entry");
           SecondBrokenCircleAnim.SetTrigger("Entry");
      //  }
    }
	
}
