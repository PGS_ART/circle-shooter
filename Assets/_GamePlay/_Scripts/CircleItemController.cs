﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class CircleItemController : MonoBehaviour {

    public int totalCircleItem;
    [SerializeField]
    private float speed;
    //Dibas
    

    private float maximumTimeRotateinaWay;

    public CircleController circleController;
    
    
    
   
    public Animator circleColorAnim;
    bool gameExit=true;
    bool isPrevouslyColord;
    float localTime;
    // Use this for initialization
    

    void Start()
    {
        localTime = Time.time;
    }
    void Update()
    {
        
        transform.Rotate(0f, 0f, speed * Time.deltaTime);

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //Debug.Log("Circle Collision");
        CameraShaker.Instance.ShakeOnce(1f, 1f, .1f, 1f);
        CircleColorAnim();
        circleController.CircleColorFillerCheck(totalCircleItem,isPrevouslyColord);
        isPrevouslyColord = true;
        
    }
    void CircleColorAnim()
    {
        //circleColorAnim.SetTrigger("Entry");
        //Sr = GetComponent<SpriteRenderer>();
        //Old = Sr.sprite;
        //Sr.sprite = Empty;
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<PolygonCollider2D>().enabled = false;
        if(Random.Range(0,2)==0)
            AudioController.Instance.Play(AudioController.Instance.GLASS_BREAK_1);
        else
            AudioController.Instance.Play(AudioController.Instance.GLASS_BREAK_2);
    }
    public void ResetCollideradnGlass()
    {
        
        //Debug.Log("Reset color Anim actual method ");
        //circleColorAnim.SetTrigger("Reset");
        isPrevouslyColord = false;
        //Sr = GetComponent<SpriteRenderer>();
        //Sr.sprite = Old;
    
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<PolygonCollider2D>().enabled = true;
    }
}
