﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class MenuController : MonoBehaviour {


    public Button adButton;
    public Animator Pillar;
    public Animator Grass1;
    public Animator Grass2;
    public Animator Cannon;
    public Animator Menu;
    public Animator endmenuanim;
    public Animator GameHud;
    public ParticleSystem BubbleTran;
    public GameObject TaptoShoot;
   

    public GameObject MusicOn;
    public GameObject MusicOff;
    public GameObject SoundOn;
    public GameObject SoundOff;

    public TextMeshProUGUI levelNumber;
    

    // Use this for initialization
    void Start () {

        AdButton();
        if (PlayerPrefs.GetInt("restart") == 1)
        {
            Debug.Log("Test");
            PlayerPrefs.SetInt("restart", 0);
            RestartGame();
        }
        else
        {
            Menu.Play("AllIn", -1, 0);
            PlayerPrefs.SetInt("restart", 0);
        }
        //if(PlayerPrefs.GetInt("musicoff") == 0)
        //    ToggleMusic.isOn = false;
        //else
        //    ToggleMusic.isOn = true;
        //if (PlayerPrefs.GetInt("soundoff") == 0)
        //    ToggleSound.isOn = false;
        //else
        //    ToggleSound.isOn = true;
        //ToggleMusic.isOn = System.Convert.ToBoolean(PlayerPrefs.GetInt("musicoff"));
        //ToggleSound.isOn = System.Convert.ToBoolean(PlayerPrefs.GetInt("soundoff"));
        //ToggleSound.isOn = false;
        //ToggleMusic

        if (PlayerPrefs.GetInt("musicoff") == 0)
            MusicOn.SetActive(true);
        else
            MusicOff.SetActive(true);
    }

    // Update is called once per frame
    void Update () {
        levelNumber.text = PlayerPrefs.GetInt("runningLevel").ToString();

    }

    void RestartGame()
    {
        AudioController.Instance.Play(AudioController.Instance.BUTTON_TAP);
        LevelController.instance.setinput(true);
        LightingController.instance.All = true;
        TaptoShoot.SetActive(true);
        Pillar.Play("Pillar", -1, 0);
        GameHud.Play("HudEntry", -1, 0);
        Grass1.Play("GrassEntry", -1, 0);
        Grass2.Play("Grass2Entry", -1, 0);
        //Cannon.Play("CannonEntry", -1, 0);
        Cannon.SetTrigger("EntryTrig");
        BubbleTran.Play();
        AudioController.Instance.Play(AudioController.Instance.BUBBLETRAN);
        LevelController.instance.StageGeneration();
        StartCoroutine("playEnu");
        AudioController.Instance.PlayGamePlayMusic(AudioController.Instance.GAME_PLAY);
    }

    public void playBut()
    {
        AudioController.Instance.Play(AudioController.Instance.BUTTON_TAP);
        LevelController.instance.setinput(true);
        LightingController.instance.All = true;
        TaptoShoot.SetActive(true);
        Pillar.Play("Pillar", -1, 0);
        GameHud.Play("HudEntry", -1, 0);
        Grass1.Play("GrassEntry", -1, 0); 
        Grass2.Play("Grass2Entry", -1, 0); 
        //Cannon.Play("CannonEntry", -1, 0);
        Cannon.SetTrigger("EntryTrig");
        Menu.Play("AllExit", -1, 0);
        BubbleTran.Play();
        AudioController.Instance.Play(AudioController.Instance.BUBBLETRAN);
        LevelController.instance.StageGeneration();
        StartCoroutine("playEnu");
        AudioController.Instance.PlayGamePlayMusic(AudioController.Instance.GAME_PLAY);

    }
    public void RestartBut()
    {

        AudioController.Instance.Play(AudioController.Instance.BUTTON_TAP);
        SceneManager.LoadScene(0);
        PlayerPrefs.SetInt("restart", 1);
      
    }

   
    public void AdButton()
    {
        adButton.onClick.AddListener(delegate
        {

            AudioController.Instance.Play(AudioController.Instance.BUTTON_TAP);
            endmenuanim.Play("EndPanelExit", -1, 0);
            LevelController.instance.setinput(true);
            LightingController.instance.All = true;
            TaptoShoot.SetActive(true);
            BubbleTran.Play();
            AudioController.Instance.Play(AudioController.Instance.BUBBLETRAN);
            LevelController.instance.StageGeneration();
            AudioController.Instance.PlayGamePlayMusic(AudioController.Instance.GAME_PLAY);

        });

       
    }
    public void HomeBut()
    {
        AudioController.Instance.Play(AudioController.Instance.BUTTON_TAP);
        SceneManager.LoadScene(0);

    }
    public void ExitBut()
    {
        Application.Quit();
    }
    IEnumerator playEnu()
    {
        yield return new WaitForSeconds(1f);
        Grass1.Play("Grass1Anim", -1, 0);
        Grass2.Play("Grass2Anim", -1, 0);
        Cannon.SetTrigger("BreathTrig");
        //Cannon.Play("Cannon", -1, 0);
    }
}
