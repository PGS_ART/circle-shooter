﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CircleController : MonoBehaviour {

    //Dibas
    //public Sprite Glass;
    //public Sprite Fishlive;
    public Sprite[] FishLiveArr;
    public Sprite[] FishDeadArr;
    //private Sprite FishDead;
    public SpriteRenderer SR;
    public ParticleSystem BloodP;
    public ParticleSystem GlassP;
    public ParticleSystem BubbleTran;
    public Animator FishAnim;
    public Animator OtherAnim;
    public Animator endmenuanim;
    
    //public GameObject G;

    int totalColor = 0;
    int totalStage = 0;
    
    public List<ChildController> childController;
    public void CircleColorFillerCheck(int totalCircleItem, bool previouslyColored)
    {
        
        if (!previouslyColored)
        {     
            totalColor++;
            GlassP.Play();
            if (totalColor == totalCircleItem)
            {
                Debug.Log(">>>>>>>>>>");
                StartCoroutine(EntryRoutine());

            }
        }
        else
        {
            //BloodP.Play();
            //StartCoroutine(GameoverRoutine());
        }
    }

    IEnumerator EntryRoutine()
    {
        Debug.Log("Come in entry routine");
        BubbleTran.Play();
        AudioController.Instance.Play(AudioController.Instance.BUBBLETRAN);
        LevelController.instance.setinput(false);
        totalColor = 0;
        int temp = childController.Count;
        //FishDead = SR.sprite; 
        //SR.sprite = Fishlive;
        SR.sprite = FishLiveArr[PlayerPrefs.GetInt("runningLevel")-1];
        FishAnim.SetTrigger("Exit");
        OtherAnim.SetTrigger("Exit");
        AudioController.Instance.Play(AudioController.Instance.Whee);
        yield return new WaitForSeconds(2f);
        SR.sprite = FishDeadArr[PlayerPrefs.GetInt("runningLevel")];
        LevelController.instance.setinput(true);
        for (int i = 0; i < temp; i++)
        {
            if (childController[i].gameObject.activeInHierarchy)
            {
                Debug.Log("Is active in hierechy++++");
                childController[i].ResetCircleItemColor(); 
                childController[i].ResetCircleItemColor(); 
            }
        }
        totalStage = PlayerPrefs.GetInt("StageNumber");
        
        PlayerPrefs.SetInt("StageNumber", ++totalStage);
        Debug.Log("Total stage +++ "+totalStage);
        Debug.Log("Total Stage ^_^_^ " + PlayerPrefs.GetInt("StageNumber"));
        yield return new WaitForSeconds(.5f);
        //GlassController.glassController.Sr.sprite = Glass;
        //GlassController.glassController.HitCount = 0;
        LevelController.instance.StageGeneration();
        Debug.Log("Level Completed");

        
        PlayerPrefs.SetInt("runningLevel", PlayerPrefs.GetInt("runningLevel") + 1);
        Debug.Log("Next Level Value  ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''  " + PlayerPrefs.GetInt("runningLevel"));
        NewStageGenController.instance.NewStageGenValue();
    }

    IEnumerator GameoverRoutine()
    {
        Debug.Log("Game Over routine");
        //Handheld.Vibrate();
        Vibration.Vibrate(200);
        AudioController.Instance.Play(AudioController.Instance.ELECTRIC);

        LevelController.instance.setinput(false);
        LightingController.instance.All = false;
        AudioController.Instance.Stop(AudioController.Instance.GAME_PLAY);
        AudioController.Instance.Play(AudioController.Instance.EndSad);
        OtherAnim.Play("ObjectsExit", -1, 0);
        FishAnim.Play("FishDeadAnimation", -1, 0);

        yield return new WaitForSeconds(1f);
        endmenuanim.Play("EndPanel", -1, 0);


        


        //SceneManager.LoadScene(0);
    }
}
