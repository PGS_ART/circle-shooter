﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanonController : MonoBehaviour {

    public GameObject prefabs;
    public Transform prefabContainer;
    //public ParticleSystem splashParticleSystem;
    //public Animator circleColor;
    public Animator Shoot;

    public ParticleSystem ShootBubble;
    public ParticleSystem Trail;
    public GameObject TaptoShoot;

    public static CanonController canonController;
	// Use this for initialization
	void Awake () {
        canonController = this;
        
	}

    

    // Update is called once per frame
    void Update () {
        if (LevelController.instance.getinput())
            if (Input.GetMouseButtonDown(0))
        {
            //Dibas
            //Shoot.Play("CannonShoot", -1, 0);
                Shoot.SetTrigger("ShootTrig");
            ShootBubble.Emit(5);
                Trail.Play();
            Instantiate(prefabs, prefabContainer);//,Quaternion.identity);
            AudioController.Instance.Play(AudioController.Instance.SHOOTING);
                TaptoShoot.SetActive(false);
        }
	}


    public void PlayParticle()
    {
        //splashParticleSystem.gameObject.SetActive(true);
        //splashParticleSystem.Play();
        //PlayAnimation();
    }
    public void StopParticle()
    {
       // splashParticleSystem.gameObject.SetActive(false);
    }
    public void PlayAnimation()
    {
        //Debug.Log("Animation");
        //circleColor.SetTrigger("Entry");
    }
}
