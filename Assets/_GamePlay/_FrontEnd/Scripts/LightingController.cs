﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightingController : MonoBehaviour {

    public static LightingController instance;

    public ParticleSystem LRU;
    public ParticleSystem LLU;
    public ParticleSystem LLM;
    public ParticleSystem LRM;
    public ParticleSystem LLD;
    public ParticleSystem LRD;
    public ParticleSystem Circle;

    public bool All = true;
    private bool gate;

    void Awake()
    {
       instance = this;
    }
    // Use this for initialization
    void Start () {
        StartCoroutine("StartL");
    }
	
	// Update is called once per frame
	void Update () {
        if(All == false && gate == false)
        {
            gate = true;
            LRU.Stop();
            LLD.Stop();
            LLU.Stop();
            LRD.Stop();
            LLM.Stop();
            LRM.Stop();
            Circle.Stop();
        }
            
	}

    IEnumerator StartL()
    {
        int a;
        while(true)
        {
            if(All == true)
            {
                gate = false;
                a = Random.Range(1, 4);
                if (a == 1)
                {
                    LRU.Play();
                    LLD.Play();
                }
                else if (a == 2)
                {
                    LLU.Play();
                    LRD.Play();
                }
                else if (a == 3)
                {
                    LLM.Play();
                    LRM.Play();
                }
                Circle.Play();
            }
            yield return new WaitForSeconds(2f);
        }
        
    } 
    
}
