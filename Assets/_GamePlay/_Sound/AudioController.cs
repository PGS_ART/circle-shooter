﻿
using UnityEngine.Audio;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioController : MonoBehaviour {

    public Sound[] sounds;
    public static AudioController Instance;
	// Use this for initialization

    public readonly String GAME_PLAY = "GamePlay";
    public readonly String BUTTON_TAP = "ButtonTap";
    public readonly String MENU_BG = "MenuBg";
    public readonly String SHOOTING = "Shoot";
    public readonly String GLASS_BREAK_1 = "GlassBreak1";
    public readonly String GLASS_BREAK_2 = "GlassBreak2";
    public readonly String HURT = "Hurt";
    public readonly String MAGIC = "Magic";
    public readonly String FLYING = "Flying";
    public readonly String SLOW_MOTION = "SlowMotion";
    public readonly String ELECTRIC = "Electric";
    public readonly String BUBBLETRAN = "BubbleTran";
    public readonly String Whee = "Whee";
    public readonly String ObsHit = "ObsHit";
    public readonly String EndSad = "EndSad";

    public GameObject MusicOn;
    public GameObject MusicOff;

    void Awake () {
        Instance = this;
        foreach(Sound s in sounds)
        {
            s.audioSource =  gameObject.AddComponent<AudioSource>();
            s.audioSource.clip = s.audioClip;
            s.audioSource.volume = s.volume;
            s.audioSource.pitch = s.pitch;
        }
	}
   

    public void Play(string name)
    {
       if(PlayerPrefs.GetInt("soundoff") == 0)
        {
            Sound s = Array.Find(sounds, sound => sound.audioClipName == name);
            s.audioSource.Play();
        }
       
    }

    public void Play(string name,bool play)
    {
        if(PlayerPrefs.GetInt("soundoff") == 0)
        {
            Sound s = Array.Find(sounds, sound => sound.audioClipName == name);
            s.audioSource.Play();
            s.audioSource.loop = play;
        }
        
    }
    public void PlayMenuBGMusic(string name)
    {

        if (PlayerPrefs.GetInt("musicoff") == 0)
        {
            Sound s = Array.Find(sounds, sound => sound.audioClipName == name);
            Stop(GAME_PLAY);
            s.audioSource.Play();
            s.audioSource.loop = true;
        }
      
    }

    public void PlayGamePlayMusic(string name)
    {


        if (PlayerPrefs.GetInt("musicoff") == 0)
        {

            Sound s = Array.Find(sounds, sound => sound.audioClipName == name);
            Stop(MENU_BG);
            s.audioSource.Play();
            s.audioSource.loop = true;
        }
        

    }


    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.audioClipName == name);
        s.audioSource.Stop();
    }

  


    public void OffSound()
    {
        if (PlayerPrefs.GetInt("soundoff") == 0)
        {   
            PlayerPrefs.SetInt("soundoff", 1);
            
        }
        else if(PlayerPrefs.GetInt("soundoff") == 1)
        {

            PlayerPrefs.SetInt("soundoff", 0);

        }
    }

    public void offMusic()
    {
        if (PlayerPrefs.GetInt("musicoff") == 0)
        {
            PlayerPrefs.SetInt("musicoff", 1);
            Stop(MENU_BG);
            MusicOn.SetActive(false);
            MusicOff.SetActive(true);
        }

        else if(PlayerPrefs.GetInt("musicoff") == 1)
        {
            PlayerPrefs.SetInt("musicoff", 0);
            PlayGamePlayMusic(MENU_BG);
            MusicOn.SetActive(true);
            MusicOff.SetActive(false);
        }
        if (PlayerPrefs.GetInt("soundoff") == 0)
        {
            PlayerPrefs.SetInt("soundoff", 1);

        }
        else if (PlayerPrefs.GetInt("soundoff") == 1)
        {

            PlayerPrefs.SetInt("soundoff", 0);

        }
    }

}
